class Property:
    """A Property represents a simple name: value pairing.

    :param name: The name of the Property
    :param value: The *optional* value of the Property

    :raises ValueError: If `name` is missing
    """

    def __init__(self, name, value=None):
        if not name:
            raise ValueError("`name` cannot be `None`")
        self._name = name
        self._value = value

    @property
    def name(self):
        """The name of the Property."""

        return self._name

    @property
    def value(self):
        """The *optional* value of the Property."""

        return self._value

    @value.setter
    def value(self, value):
        """The *optional* value of the Property."""

        self._value = value

    def __str__(self):
        """A str representation of this Property."""

        return f"{self.__class__.__name__}(name: {self.name}, value: {self.value})"
