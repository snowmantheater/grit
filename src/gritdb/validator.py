from abc import ABC, abstractmethod


class EntityValidator(ABC):
    """An EntityValidator is an interface which needs to be implemented by any
    class used to verify values, types or other configurations of any Entity."""

    @abstractmethod
    def validate(self, entity, create, delete):
        """Validate an Entity based on a set of rules.

        :param entity: The Entity to be evaluated
        :param create: Whether the validation is for a *create* call
        :param delete: Whether the validation is for a *delete* call

        :raises ValueError: If any criteria for validation are not met
        """

        pass


class DefaultEntityValidator(EntityValidator):
    """A EntityValidator which simply calls each Column's validation."""

    def validate(self, entity, create, delete):
        for column in entity.columns:
            column.validate(create, delete)
