from abc import ABC, abstractmethod


class RowMapper(ABC):
    """A RowMapper is an *abstract* class the needs to be extended by all row
    mappers. It takes a connection and a depth to controll the transactional
    reading of *deeper* object layers.

    :param connection: The transactional Connection
    :param depth: How many more depth levels to read
    """

    def __init__(self, connection=None, depth=-1):
        self._connection = connection
        self._depth = depth

    @property
    def connection(self):
        """The transactional Connection to pass to Dao calls for reading
        deeper."""

        return self._connection

    @property
    def depth(self):
        """How many more depth levels to read."""

        return self._depth

    @property
    def next_depth(self):
        """Used to pass to to subsequent calls in order to reduce the depth by
        one."""

        return self._depth - 1 if self._depth else 0

    @property
    def expand(self):
        """Whether or not the depth level indicates to expand at this level."""

        return self.depth != 0

    @abstractmethod
    def maprow(self, row):
        """Given a Row, return the mapped Entity which represents it."""

        return None
