from collections import namedtuple

Key = namedtuple('Key', ['child', 'parent'])


class Relationship:
    """Represents a *relationship* between a *parent* and *child* Entity pair.
    Keys may be passed as a single str or a (str, str). A single str creates a
    Key where the child and parent key names are the same. Passing a pair, the
    first str is the child's key name and the second str is the parent's key
    name.

    :param keys: The keys which bind a parent Entity to the child Entity. These
        may be passed as a single str or as (str, str) pairs.

    :raise ValueError: If there are no keys passed, or if any key is not a str
        or a (str, str)
    """

    def __init__(self, *keys):
        if not keys:
            raise ValueError(f"{self.__class__.__name__} must define at least one key")

        k = []
        for key in keys:
            if type(key) is str:
                k.append(Key(key, key))
            elif type(key) is tuple and len(key) == 2:
                k.append(Key(key[0], key[1]))
            else:
                raise ValueError("Keys must be either `str` or `(str, [str])`")

        self._keys = tuple(k)

    @property
    def keys(self):
        """The parsed keys of this Relationship."""

        return self._keys

    def __str__(self):
        """A str representation of this Relationship."""

        keys = map(str, self.keys)
        return f"{self.__class__.__name__}(keys: [{', '.join(keys)}])"


class Singular(Relationship):
    """A Singular Relationship means the parent Entity has a foreign key(s)
    reference to a single child Entity. Keys may be passed as a single str or a
    (str, str). A single str creates a Key where the child and parent key names
    are the same. Passing a pair, the first str is the child's key name and the
    second str is the parent's key name.

    :param keys: The keys which bind a parent Entity to the child Entity. These
        may be passed as a single str or as (str, str) pairs.

    :raise ValueError: If there are no keys passed, or if any key is not a str
        or a (str, str)
    """

    def __init__(self, *keys):
        super().__init__(*keys)


class Plural(Relationship):
    """A Plural Relationship means child Entitys have a foreign key reference
    back to a single parent Entity. Keys may be passed as a single str or a
    (str, str). A single str creates a Key where the child and parent key names
    are the same. Passing a pair, the first str is the child's key name and the
    second str is the parent's key name.

    :param keys: The keys which bind a parent Entity to the child Entity. These
        may be passed as a single str or as (str, str) pairs.

    :raise ValueError: If there are no keys passed, or if any key is not a str
        or a (str, str)
    """

    def __init__(self, *keys):
        super().__init__(*keys)
