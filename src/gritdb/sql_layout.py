from warnings import warn
from sqlalchemy import text

PREFIX_CREATE = "-- CREATE"
PREFIX_READ = "-- READ"
PREFIX_UPDATE = "-- UPDATE"
PREFIX_DELETE = "-- DELETE"
PREFIX_STATEMENT = "-- STATEMENT"


class SqlCollection:
    """Creates a new, empty SqlCollection."""

    def __init__(self):
        self._collection = {}

    @property
    def collection(self):
        """The internal collection of SQL to entity (criteri) mappings."""

        return self._collection

    @staticmethod
    def _parse_key(key):
        """Parse a `key` which can be anything like:
          + entity
          + entity, criteria
          + entity, (criteria
        The result will always be: entity, (criteria, ...) where the criteria
        tuple always exists, even if it is empty.

        :param key: The key combination to parse

        :returns: A str, (str, ...) parsed value

        :raises KeyError: If the key is of an invalid format
        """

        if type(key) != tuple:
            return key, tuple()

        if len(key) == 1:
            return key[0], tuple()

        if len(key) == 2:
            entity = key[0]
            criteria = key[1]
            if type(criteria) != tuple:
                criteria = (criteria,)
            return entity, criteria

        raise KeyError("`key` must be: `entity[, (criteria, [...])]`")

    def __getitem__(self, key):
        """Returns the SQL for a given composite key as a TextClause if exists.

        :param key: A composite key of an enity name plus optional criteria

        :returns: A TextClause of the SQL for the given entity and criteria

        :raises IndexError: If the requested key does not exist
        """

        entity, criteria = self._parse_key(key)
        if (entity, criteria) not in self.collection:
            raise IndexError(f"('{entity}', {criteria}) SQL does not exist.")
        return self.collection[(entity, criteria)]

    def __setitem__(self, key, sql):
        """Sets the SQL for a given composite key. If it exists, a warning is
        raised to indicate that an unexpected overwrite is happening.

        :param key: A composite key of an entiy name plus optional criteria
        :param sql: The SQL to set for the key

        :raises RuntimeWarning: If an existing key is being overwritten
        """

        entity, criteria = self._parse_key(key)
        if (entity, criteria) in self.collection:
            warn(RuntimeWarning(f"('{entity}', {criteria}) SQL already exists; unexpected overwrite."))
        self.collection[(entity, criteria)] = text(sql)

    def __str__(self):
        """A str representation of this SqlCollection."""
        result = "SqlCollection(["
        for key in self.collection.keys():
            result += f"criteria({key}), "
        return result[:-2] + "])"


class SqlLayout:
    def __init__(self, path):
        self._create = SqlCollection()
        self._read = SqlCollection()
        self._update = SqlCollection()
        self._delete = SqlCollection()
        self._statement = SqlCollection()
        self._load(path)

    @property
    def create(self):
        """The `create` SqlCollection."""

        return self._create

    @property
    def read(self):
        """The `read` SqlCollection."""

        return self._read

    @property
    def update(self):
        """The `update` SqlCollection."""

        return self._update

    @property
    def delete(self):
        """The `delete` SqlCollection."""

        return self._delete

    @property
    def statement(self):
        """The generic `statement` SqlCollection."""

        return self._statement

    @staticmethod
    def _is_header(line):
        """Given a line of text, determine if it is a SQL statement header.

        :param line: The given line of text

        :returns: Whether it is header
        """

        if line.startswith(PREFIX_CREATE):
            return True
        if line.startswith(PREFIX_READ):
            return True
        if line.startswith(PREFIX_UPDATE):
            return True
        if line.startswith(PREFIX_DELETE):
            return True
        if line.startswith(PREFIX_STATEMENT):
            return True
        return False

    @staticmethod
    def _is_terminal(line):
        """Given a line of SQL, determine if it is a terminal line, contianing a
        semi-colon.

        :param line: The given line of SQL

        :returns: Whether this line of SQL is a terminal line
        """

        if line[-1:] == ";":
            return True
        return False

    @staticmethod
    def _parse_criteria(criteria):
        """Parse the criteria portion of a header line of text. The keys must be
        surround by parentheses (like tuple syntax in code).

        :param criteria: The criteria portion of text

        :returns: A tuple of keys; possibly

        :raises ValueError: If the criteria portion is not surround by parens
        """

        if criteria[0] != "(" and criteria[-1] != ")":
            raise ValueError("Criteria must be surrounded by (..)")
        criteria = tuple([c.strip() for c in criteria.strip()[1:-1].split(",")])
        return criteria

    def _parse_header(self, line):
        """Parse the header text into a Collection Group, Entity Name and an
        optional Criteria tuple of Key Names.

        :param line: The given header text

        :returns: The parsed elements of a header

        :raises ValueError: If the header is badly formed
        """

        if line.startswith(PREFIX_CREATE):
            collection = self.create
            line = line[len(PREFIX_CREATE):]
        elif line.startswith(PREFIX_READ):
            collection = self.read
            line = line[len(PREFIX_READ):]
        elif line.startswith(PREFIX_UPDATE):
            collection = self.update
            line = line[len(PREFIX_UPDATE):]
        elif line.startswith(PREFIX_DELETE):
            collection = self.delete
            line = line[len(PREFIX_DELETE):]
        elif line.startswith(PREFIX_STATEMENT):
            collection = self.statement
            line = line[len(PREFIX_STATEMENT):]
        else:
            raise ValueError(f"'{line}' is not a valid header.")

        line = line.strip().split(" ", 1)
        entity = line[0]

        if len(line) == 2:
            criteria = self._parse_criteria(line[1])
        else:
            criteria = tuple()

        return collection, entity, criteria

    def _load(self, path):
        """Load a SQL layout from the given path.

        :param path: A path (either str or Path) to the file containing the SQL
        """
        with open(path) as file:
            state = "default"
            collection = None
            entity = None
            criteria = None
            sql = None

            for line in file:
                line = line.strip()

                if state == "default" and self._is_header(line):
                    state = "parsing"
                    collection, entity, criteria = self._parse_header(line)
                    sql = []

                elif state == "parsing":
                    line = line.split("--")[0].rstrip()
                    if line:
                        terminate = False
                        if self._is_terminal(line):
                            terminate = True
                            line = line[:-1]
                        sql.append(line)
                        if terminate:
                            collection[(entity, criteria)] = " ".join(sql).strip()
                            collection = None
                            entity = None
                            criteria = None
                            sql = None
                            state = "default"

    def __str__(self):
        """A str representation of this SqlLayout."""

        result = "SqlPlan(\n"
        result += f"  creates={self.create},\n"
        result += f"  reads={self.read},\n"
        result += f"  updates={self.update},\n"
        result += f"  deletes={self.delete},\n"
        result += f"  statemeents={self.statement}\n)"
        return result
