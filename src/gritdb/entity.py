from .column import Column
from .validator import DefaultEntityValidator


class Entity:
    """An Entity represents basically a *table* in the database. It is made up
    of Columns and their values.

    :param name: The Entity's name; which aligns to entity names in the
        SqlLayouts
    """

    def __init__(self, name):
        self._name = name
        self._columns = {}
        self._validators = [DefaultEntityValidator()]

    @property
    def name(self):
        """This Entity's name."""

        return self._name

    @property
    def columns(self):
        """The Columns that make up this Entity."""

        return tuple(self._columns.values())

    @property
    def validators(self):
        """The EntityValidators used to validate this Entity."""

        return self._validators

    @property
    def column_names(self):
        """The names of the Columns that make up this Entity."""

        return tuple(self._columns.keys())

    def validator(self, validator):
        """Adds `validator` to this Entity's list of validators.

        :param validator: The EntityValidator to add to the validator list
        """

        self._validators.append(validator)

    def column(self, name, value=None, required=False, key=False, size=None, related=None):
        """Add a new Column to this Entity.

        :param name: The name of the Column
        :param value: The *optional* value of the Column
        :param required: Whether this Column is required
        :param key: Whether this Column is part of the primary key
        :param size: The *optional* two-part size constraint of this Column
        :param related: The Relationship this Column's Entity has with the
            parent Entity

        :raises ValueError: If both `related` and `key` are set, size is not int
            or (int, int) or `related` is not Singular or Plural
        """

        self._columns[name] = Column(name, value, required, key, size, related)

    def reparent(self, column_name):
        """For each child Entity, if it is Plural related, assign the value for
        the parent's key to the child's foreign key. This effectively makes this
        Entity the parent of each child Entity.

        :param column_name: The name of Plural Column to *reparent*
        """

        column = self._columns[column_name]
        if column.isplural:
            for entity in column.value:
                for fk in column.foreign_keys:
                    entity[fk.child] = self[fk.parent]

    def validate(self, create=False, delete=False):
        """Using default *and* auxiliary EntityValidators, validate each of the
        Columns in this Entity based on the intent (as expressed by `create` and
        `delete` values.

        :param create: Whether to validate for a *create* operation
        :param delete: Whether to validate for a *delete* operation

        :raises ValueError: For any validation rule exceptions
        """

        for validator in self.validators:
            validator.validate(self, create, delete)

    def params(self, keys=False, fields=False):
        """Return a dictionary of key/value pairs for the values that make up
        this Entity.

        :param keys: Whether to include Columns marked as keys
        :param fields: Whether to include Columns not marked as keys

        :return: A dictionary of key/value pairs suitable as query params
        """

        if not keys and not fields:
            return {}

        params = {}
        for name, column in self._columns.items():
            if (keys and column.iskey) or (fields and not column.iskey):
                if column.isrelated:
                    if column.issingular:
                        for fk in column.foreign_keys:
                            if column.value:
                                params[fk.parent] = column.value[fk.child]
                            else:
                                params[fk.parent] = None
                else:
                    params[name] = column.value

        return params

    def __contains__(self, column_name):
        """Determines if `column_name` is a valid column name in this Entity.

        :param column_name: The column name to validate
        """

        return column_name in self._columns

    def __getitem__(self, column_name):
        """Returns the value of the Column named `column_name`.

        :param column_name: The name of the column to retrieve

        :returns: The value of the Column named `column_name`

        :raises IndexError: If the column name does not exist in this Entity
        """

        if column_name not in self:
            raise IndexError(f"No such column '{column_name}' for Entity[{self.name}]")
        return self._columns[column_name].value

    def __setitem__(self, column_name, value):
        """Sets the value of the Column named `column_name` to `value`.

        :param column_name: The name of the column to set
        :param value: The value to set the Column value to

        :raises IndexError: If the column name does not exist in this Entity
        :raises ValueError: If trying to set a plural child Column to a non-list
        """

        if column_name not in self:
            raise IndexError(f"No such column '{column_name}' for Entity[{self.name}]")
        column = self._columns[column_name]
        if column.isplural:
            if type is not None and type(value) is not list:
                raise ValueError(f"Plural column {column_name} must be a list")

        self._columns[column_name].value = value

    def __str__(self):
        """Returns a str representation of this Entity."""

        result = f"Entity[{self.name}]("
        for name, value in self.params(keys=True, fields=True).items():
            result += f"{name}: {value}, "
        return result[:-2] + ")"
