from .property import Property
from .relationship import Plural, Singular


class Column(Property):
    """A Column is a Property of an Entity. It has multiple meta values
    associated with it.

    :param name: The name of the Column
    :param value: The *optional* value of the Column
    :param required: Whether this Column is required; default is False
    :param key: Whether this Column is part of the primary key; default is False
    :param size: The *optional* two-part size constraint of this Column
    :param related: The *relationship* this Column's Entity has with the parent
        Entity

    :raises ValueError: If both related and key are set, size is not int or
        (int, int) or related is not Singular or Plural
    """

    def __init__(self, name, value=None, required=False, key=False, size=None, related=None):
        super().__init__(name, value)

        self._isrequired = required
        self._iskey = key

        if related and key:
            raise ValueError("Relationships cannot be defined on a key Column")

        if size:
            if type(size) is int:
                self._size = (size,)
            elif type(size) is tuple and len(size) == 2:
                self._size = size
            else:
                raise ValueError("`length` must be of type `int` or `(int, int)`; or `None`")
        else:
            self._size = None

        if related:
            if related.__class__ is Plural or related.__class__ is Singular:
                self._related = related
            else:
                raise ValueError("`related` must either be `Plural` or `Singular`")
        else:
            self._related = related

    @property
    def isrequired(self):
        """Whether this Column is required."""

        return self._isrequired

    @property
    def iskey(self):
        """Whether this Column is part of its Entity's primary key."""

        return self._iskey

    @property
    def size(self):
        """The size constraint on this Column."""

        if self._size:
            return self._size[0]
        else:
            return None

    @property
    def size_d(self):
        """The decimal size constraint on this Column."""

        if self._size and len(self._size) == 2:
            return self._size[1]
        else:
            return None

    @property
    def isrelated(self):
        """Whether and how this column is related to the parent Entity."""

        return self._related is not None

    @property
    def isplural(self):
        """Whether this column is Pluraly related with its parent."""

        return self.isrelated and self._related.__class__ is Plural

    @property
    def issingular(self):
        """Whether this column is Singularly related with its parent."""

        return self.isrelated and self._related.__class__ is Singular

    @property
    def foreign_keys(self):
        """The foreign keys defining the relationship between this Entity and
        its parent."""

        if self.isrelated:
            return self._related.keys
        else:
            return None

    def validate(self, create=False, delete=False):
        """Validates whether this column has data to match the constraints.

        :param create: Validate as if this column was for a create call
        :param delete: Validate as if this column was for a delete call

        :raises ValueError: If any criteria for validation are not met
        """

        if create and delete:
            raise ValueError("`create` and `delete` are mutually exclusive.")

        if create:
            if self.isrequired and self.value is None:
                raise ValueError(f"'{self.name}' is required for create.")
        elif delete:
            if self.iskey and self.value is None:
                raise ValueError(f"'{self.name}' is required for delete.")
        else:
            if (self.isrequired or self.iskey) and self.value is None:
                raise ValueError(f"'{self.name}' is required.")

        if self.value is None:
            return

        v = str(self.value)

        if self.size and self.size_d:
            number = v.split(".")

            if len(number[0]) > self.size:
                raise ValueError(f"Left side digits of '{self.name}' cannot be longer than {self.size}")

            if len(number) > 1 and len(number[1]) > self.size_d:
                raise ValueError(f"Right side digits of '{self.name}' cannot be longer than {self.size}")
        elif self.size:
            if len(v) > self.size:
                raise ValueError(f"'{self.name}' cannot be longer than {self.size}")

    def __str__(self):
        """A str representation of this Column."""

        result = f"Column(name: {self.name}, value: {self.value}, "
        if self.isrequired:
            result += "isrequired, "
        if self.iskey:
            result += "iskey, "
        if self.size:
            result += f"size: {self.size}, "
        if self.size_d:
            result += f"size_d: {self.size_d}, "
        if self.isrelated:
            result += f"related: {self._related}, "
        return result[:-2] + ")"
