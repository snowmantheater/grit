import functools


def transactional(operation):
    """A transactionl operation will automatically receive a connection and
    depth level. These are generated automatically. If a keyword argument `tx`
    is passed with a connection, that connection is used, otherwise the engine
    is asked for a new connection. If a keyword argument `depth` is passed then
    that depth is set, otherwise 0 is the default. If a keyword argument
    `expand` is passed then the depth is set to -1, which is infinite. All other
    arguments define by the method are passed as is.
    """

    @functools.wraps(operation)
    def wrapper(*args, **kwargs):
        if 'tx' not in kwargs:
            connection = args[0].engine.connect()
            tx = False
        else:
            connection = kwargs['tx']
            del kwargs['tx']
            tx = True

        if 'depth' not in kwargs:
            depth = 0
        else:
            depth = kwargs['depth']
            del kwargs['depth']

        if 'expand' in kwargs and kwargs['expand']:
            depth = -1
            del kwargs['expand']

        args = args[0], connection, depth, *args[1:]
        try:
            result = operation(*args, **kwargs)
            if not tx:
                connection.commit()
        finally:
            if not tx:
                connection.close()
        return result
    return wrapper


class Dao:
    """A Dao provides utility methods for queries and statements, helper methods
    for managing SqlLayout access and depth management.

    :param engine: The SqlAlchemy Engine which manages connections
    :param entity_name: The name that this Dao defaults to for SqlLayout lookups
    :param sql_layout: The collection of SQL statements used by this Dao
    """

    def __init__(self, engine, entity_name, sql_layout):
        self._engine = engine
        self._entity_name = entity_name
        self._sql_layout = sql_layout

    @property
    def engine(self):
        """The SqlAlchemy Engine used by this Dao."""

        return self._engine

    @property
    def entity_name(self):
        """The default name used by this Dao to discover SQL."""

        return self._entity_name

    @property
    def sql(self):
        """The SqlLayout instance associated with this Dao."""

        return self._sql_layout

    @staticmethod
    def expand(depth):
        """Whether to process deeper layers of this Dao's Entity."""

        return depth != 0

    @staticmethod
    def next_depth(depth):
        """The next depth level to pass to subsequent Dao or Mapper calls."""

        return depth - 1 if depth else 0

    def get_statement(self, criteria=None, entity_name=None):
        """Returns the statement associtead with the criter and default entity name. If `entity_name` is provided, then
        use that instead of the default.

        :param criteria: The criteria of the SQL key
        :param entity_name: An overriding entity name

        :return: The associated SQL
        """

        entity_name = entity_name or self.entity_name
        if criteria:
            return self.sql.statement[entity_name]
        else:
            return self.sql.statement[entity_name, tuple(criteria.keys())]

    def exec_create(self, connection, params=None, entity_name=None):
        """Creates a new entity from the given `params`. If `entity_name` is provided, then use that instead of the
        default. A typical create does not have criteria, so they are not received by this method.

        :param connection: The SqlAlchemy Connection to use
        :param params: The parameters to use supplied as query parameters
        :param entity_name: An overriding entity name

        :return: The last row id / primary key; not used if the PK was provided in `params`
        """

        entity_name = entity_name or self.entity_name
        sql = self.sql.create[entity_name]
        if not params:
            result = connection.execute(sql)
        else:
            result = connection.execute(sql, params)

        return result.lastrowid

    def exec_read(self, connection, criteria=None, mapper=None, entity_class=None, validate=True, entity_name=None):
        """Reads a number of Rows from the database using the supplied `criteria`. If `entity_name` is provided, then
        use that instead of the default. Either a Row mapper or Entity class must be provided to map the Rows into a
        list for returning. If `validate` is True, then each Entity will be validated before being added to the list.

        :param connection: The SqlAlchemy Connection to use
        :param criteria: The criteria to use supplied as query paramaters
        :param mapper: If supplied, used to map each Row into an Entity
        :param entity_class: If supplied, used tomap each Row into this Entity
        :param validate: Whether to validate each Entity before adding to the list
        :param entity_name: An overriding entity name

        :return: All Rows mapped as Entities in a list

        :raises ValueError: If both `mapper` and `entity_class` or neither are supplied
        """

        entity_name = entity_name or self.entity_name
        if not mapper and not entity_class:
            raise ValueError("Either `mapper` or `entity_class` must be specified.")

        rows = []

        if not criteria:
            sql = self.sql.read[entity_name]
            result = connection.execute(sql)
        else:
            sql = self.sql.read[entity_name, tuple(criteria.keys())]
            result = connection.execute(sql, criteria)

        column_names = tuple(result.keys())
        for row in result:
            if entity_class:
                entity = entity_class()
                for column_name in column_names:
                    entity[column_name] = row.__getattribute__(column_name)
            else:
                entity = mapper.maprow(row)

            if validate:
                entity.validate()
            rows.append(entity)

        return rows

    def exec_read_first(
            self,
            connection,
            criteria=None,
            mapper=None,
            entity_class=None,
            validate=True,
            entity_name=None
    ):
        """Reads the first Row from the database using the supplied `criteria`. If `entity_name` is provided, then
        use that instead of the default. Either a Row mapper or Entity class must be provided to map the Row for
        returning. If `validate` is True, then the Entity will be validated before being returned.

        :param connection: The SqlAlchemy Connection to use
        :param criteria: The criteria to use supplied as query paramaters
        :param mapper: If supplied, used to map each Row into an Entity
        :param entity_class: If supplied, used tomap each Row into this Entity
        :param validate: Whether to validate each Entity before adding to the list
        :param entity_name: An overriding entity name

        :return: The first Row mapped as Entity

        :raises ValueError: If both `mapper` and `entity_class` or neither are supplied
        """

        entity_name = entity_name or self.entity_name
        if not mapper and not entity_class:
            raise ValueError("Either `mapper` or `entity_class` must be specified.")

        if not criteria:
            sql = self.sql.read[entity_name]
            result = connection.execute(sql)
        else:
            sql = self.sql.read[entity_name, tuple(criteria.keys())]
            result = connection.execute(sql, criteria)

        column_names = tuple(result.keys())
        row = result.first()

        if not row:
            return None
        elif entity_class:
            entity = entity_class()
            for column in column_names:
                entity[column] = row.__getattribute__(column)
        else:
            entity = mapper.maprow(row)

        if validate:
            entity.validate()

        return entity

    def exec_update(self, connection, criteria=None, params=None, entity_name=None):
        """Updates rows in the database using the supplied `criteria` and `params`. If `entity_name` is provided, then
        use that instead of the default.

        :param connection: The SqlAlchemy Connection to use
        :param criteria: The criteria to use supplied as query paramaters
        :param params: The params to use supplied as query parameters
        :param entity_name: An overriding entity name

        :return: The number of rows affected by this update action
        """

        entity_name = entity_name or self.entity_name
        params = params if params else {}
        if not criteria:
            sql = self.sql.update[entity_name]
        else:
            params.update(criteria)
            sql = self.sql.update[entity_name, tuple(criteria.keys())]

        if not params:
            result = connection.execute(sql)
        else:
            result = connection.execute(sql, params)

        return result.rowcount

    def exec_delete(self, connection, criteria=None, entity_name=None):
        """Deletes rows in the database using the supplied `criteria`. If `entity_name` is provided, then use that
        instead of the default.

        :param connection: The SqlAlchemy Connection to use
        :param criteria: The criteria to use supplied as query paramaters
        :param entity_name: An overriding entity name

        :return: The number of rows affected by this delete action
        """

        entity_name = entity_name or self.entity_name
        if not criteria:
            sql = self.sql.delete[entity_name]
            result = connection.execute(sql)
        else:
            sql = self.sql.delete[entity_name, tuple(criteria.keys())]
            result = connection.execute(sql, criteria)
        return result.rowcount
