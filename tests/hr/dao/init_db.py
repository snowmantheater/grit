from pathlib import Path
import pytest
from sqlalchemy import create_engine, text


@pytest.fixture
def engine():
    engine = create_engine("sqlite+pysqlite:///:memory:")
    with engine.connect() as connection:
        with open(Path(__file__).parent / "test-db.create-and-prime.sql") as statements:
            for statement in statements:
                connection.execute(text(statement))
        connection.commit()
    return engine
