from datetime import date
import pytest
from .init_db import engine
from hr.dao import EmployeeDao


@pytest.fixture
def employee_dao(engine):
    return EmployeeDao(engine)


def test_find_all(employee_dao):
    employees = employee_dao.find_all()
    assert len(employees) == 40


def test_find_by_id_where_id_is_101(employee_dao):
    employee = employee_dao.find_by_id(101)
    assert employee['employee_id'] == 101
    assert employee['first_name'] == "Neena"
    assert employee['last_name'] == "Kochhar"
    assert employee['email'] == "neena.kochhar@sqltutorial.org"
    assert employee['phone_number'] == "515.123.4568"
    assert employee['hire_date'] == date(1989, 9, 21)
    assert employee['salary'] == 17000
    assert employee['department']['department_id'] == 9
    assert employee['department']['location'] is None
    assert employee['job']['job_id'] == 5
    assert employee['job']['job_title'] is None
    assert employee['manager']['employee_id'] == 100
    assert employee['manager']['first_name'] is None
    assert len(employee['dependents']) == 0


def test_find_by_id_where_id_is_100_and_expand(employee_dao):
    employee = employee_dao.find_by_id(101, expand=True)
    assert employee['employee_id'] == 101
    assert employee['first_name'] == "Neena"
    assert employee['last_name'] == "Kochhar"
    assert employee['email'] == "neena.kochhar@sqltutorial.org"
    assert employee['phone_number'] == "515.123.4568"
    assert employee['hire_date'] == date(1989, 9, 21)
    assert employee['salary'] == 17000

    assert employee['department']['department_id'] == 9
    assert employee['department']['department_name'] == "Executive"

    assert employee['department']['location']['location_id'] == 1700
    assert employee['department']['location']['street_address'] == "2004 Charade Rd"
    assert employee['department']['location']['postal_code'] == "98199"
    assert employee['department']['location']['city'] == "Seattle"
    assert employee['department']['location']['state_province'] == "Washington"

    assert employee['department']['location']['country']['country_id'] == "US"
    assert employee['department']['location']['country']['country_name'] == "United States of America"

    assert employee['department']['location']['country']['region']['region_id'] == 2
    assert employee['department']['location']['country']['region']['region_name'] == "Americas"

    assert employee['job']['job_id'] == 5
    assert employee['job']['job_title'] == "Administration Vice President"
    assert employee['job']['min_salary'] == 15000
    assert employee['job']['max_salary'] == 30000

    assert employee['manager']['employee_id'] == 100
    assert employee['manager']['first_name'] is None

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5
    assert employee['dependents'][0]['first_name'] == "Johnny"
    assert employee['dependents'][0]['last_name'] == "Kochhar"
    assert employee['dependents'][0]['relationship'] == "Child"
    assert employee['dependents'][0]['employee_id'] == 101


def test_find_by_id_where_id_is_100_and_depth_is_1(employee_dao):
    employee = employee_dao.find_by_id(101, depth=1)
    assert employee['employee_id'] == 101

    assert employee['department']['department_id'] == 9

    assert employee['department']['location']['location_id'] == 1700
    assert employee['department']['location']['street_address'] is None
    assert employee['department']['location']['postal_code'] is None
    assert employee['department']['location']['city'] is None
    assert employee['department']['location']['state_province'] is None
    assert employee['department']['location']['country'] is None

    assert employee['job']['job_id'] == 5

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5


def test_find_by_id_where_id_is_100_and_depth_is_2(employee_dao):
    employee = employee_dao.find_by_id(101, depth=2)
    assert employee['employee_id'] == 101

    assert employee['department']['department_id'] == 9

    assert employee['department']['location']['location_id'] == 1700

    assert employee['department']['location']['country']['country_id'] == "US"
    assert employee['department']['location']['country']['country_name'] is None

    assert employee['department']['location']['country']['region'] is None

    assert employee['job']['job_id'] == 5

    assert employee['manager']['employee_id'] == 100

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5


def test_find_by_id_where_id_is_100_and_depth_is_3(employee_dao):
    employee = employee_dao.find_by_id(101, depth=3)
    assert employee['employee_id'] == 101

    assert employee['department']['department_id'] == 9

    assert employee['department']['location']['location_id'] == 1700

    assert employee['department']['location']['country']['country_id'] == "US"

    assert employee['department']['location']['country']['region']['region_id'] == 2
    assert employee['department']['location']['country']['region']['region_name'] is None

    assert employee['job']['job_id'] == 5

    assert employee['manager']['employee_id'] == 100

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5


def test_find_by_id_where_id_is_100_and_depth_is_4(employee_dao):
    employee = employee_dao.find_by_id(101, depth=4)
    assert employee['employee_id'] == 101

    assert employee['department']['department_id'] == 9

    assert employee['department']['location']['location_id'] == 1700

    assert employee['department']['location']['country']['country_id'] == "US"

    assert employee['department']['location']['country']['region']['region_id'] == 2
    assert employee['department']['location']['country']['region']['region_name'] == "Americas"

    assert employee['job']['job_id'] == 5

    assert employee['manager']['employee_id'] == 100

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5


def test_find_by_id_where_id_is_100_and_depth_is_5(employee_dao):
    pass


def test_find_by_id_where_id_is_999(employee_dao):
    employee = employee_dao.find_by_id(999)
    assert employee is None

    employee = employee_dao.find_by_id(999, expand=True)
    assert employee is None
