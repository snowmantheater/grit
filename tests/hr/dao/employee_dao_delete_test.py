import pytest
from .init_db import engine
from hr.dao import EmployeeDao, DependentDao
from hr.entity import Employee


@pytest.fixture
def employee_dao(engine):
    return EmployeeDao(engine)


@pytest.fixture
def dependent_dao(engine):
    return DependentDao(engine)


def test_delete_flat_where_id_is_101(employee_dao, dependent_dao):
    employee = Employee()
    employee['employee_id'] = 101

    affected = employee_dao.delete(employee)
    assert affected == (1, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 39

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_flat_where_id_is_101_and_expand(employee_dao, dependent_dao):
    employee = Employee()
    employee['employee_id'] = 101

    affected = employee_dao.delete(employee, expand=True)
    assert affected == (1, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 39

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_expanded_where_id_is_101(employee_dao, dependent_dao):
    employee = employee_dao.find_by_id(101, expand=True)

    affected = employee_dao.delete(employee)
    assert affected == (1, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 39

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_expanded_where_id_is_101_and_expand(employee_dao, dependent_dao):
    employee = employee_dao.find_by_id(101, expand=True)

    affected = employee_dao.delete(employee, expand=True)
    assert affected == (1, 1)

    employees = employee_dao.find_all()
    assert len(employees) == 39

    dependents = dependent_dao.find_all()
    assert len(dependents) == 29


def test_delete_flat_where_id_is_999(employee_dao, dependent_dao):
    employee = Employee()
    employee['employee_id'] = 999

    affected = employee_dao.delete(employee)
    assert affected == (0, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_flat_where_id_is_999_and_expand(employee_dao, dependent_dao):
    employee = Employee()
    employee['employee_id'] = 999

    affected = employee_dao.delete(employee, expand=True)
    assert affected == (0, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_expanded_where_id_is_999(employee_dao, dependent_dao):
    employee = employee_dao.find_by_id(101)
    employee['employee_id'] = 999

    affected = employee_dao.delete(employee)
    assert affected == (0, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_delete_expanded_where_id_is_999_and_expand(employee_dao, dependent_dao):
    employee = employee_dao.find_by_id(101)
    employee['employee_id'] = 999

    affected = employee_dao.delete(employee, expand=True)
    assert affected == (0, 0)

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30
