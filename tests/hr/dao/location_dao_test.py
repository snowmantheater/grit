import pytest
from .init_db import engine
from hr.dao import LocationDao, CountryDao
from hr.entity import Location


@pytest.fixture
def location_dao(engine):
    return LocationDao(engine)


@pytest.fixture
def country_dao(engine):
    return CountryDao(engine)


def test_find_all(location_dao):
    locations = location_dao.find_all()
    assert len(locations) == 7


def test_find_by_id_where_id_is_2400(location_dao):
    location = location_dao.find_by_id(2400)
    assert location['location_id'] == 2400
    assert location['street_address'] == "8204 Arthur St"
    assert location['postal_code'] is None
    assert location['city'] == "London"
    assert location['state_province'] is None
    assert location['country']['country_id'] == "UK"
    assert location['country']['country_name'] is None
    assert location['country']['region'] is None


def test_find_by_id_where_id_is_1400(location_dao):
    location = location_dao.find_by_id(1400)
    assert location['location_id'] == 1400
    assert location['street_address'] == "2014 Jabberwocky Rd"
    assert location['postal_code'] == "26192"
    assert location['city'] == "Southlake"
    assert location['state_province'] == "Texas"
    assert location['country']['country_id'] == "US"
    assert location['country']['country_name'] is None
    assert location['country']['region'] is None


def test_find_by_id_where_id_is_1400_and_expand(location_dao):
    location = location_dao.find_by_id(1400, expand=True)
    assert location['location_id'] == 1400
    assert location['country']['country_id'] == "US"
    assert location['country']['country_name'] == "United States of America"
    assert location['country']['region']['region_id'] == 2
    assert location['country']['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_1400_and_depth_is_1(location_dao):
    location = location_dao.find_by_id(1400, depth=1)
    assert location['location_id'] == 1400
    assert location['country']['country_id'] == "US"
    assert location['country']['country_name'] == "United States of America"
    assert location['country']['region']['region_id'] == 2
    assert location['country']['region']['region_name'] is None


def test_find_by_id_where_id_is_1400_and_depth_is_2(location_dao):
    location = location_dao.find_by_id(1400, depth=2)
    assert location['location_id'] == 1400
    assert location['country']['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_1234(location_dao):
    location = location_dao.find_by_id(1234)
    assert location is None

    location = location_dao.find_by_id(1234, expand=True)
    assert location is None


def test_create(location_dao, country_dao):
    country = country_dao.find_by_id('US')
    new_location = Location()
    new_location['street_address'] = "123 Test St."
    new_location['postal_code'] = "12345"
    new_location['city'] = "Test City"
    new_location['state_province'] = "Test State"
    new_location['country'] = country

    location_dao.create(new_location)
    assert new_location['location_id'] == 2701

    location = location_dao.find_by_id(2701)
    assert location['location_id'] == 2701
    assert location['street_address'] == "123 Test St."
    assert location['postal_code'] == "12345"
    assert location['city'] == "Test City"
    assert location['state_province'] == "Test State"
    assert location['country']['country_id'] == "US"


def test_update_where_id_is_1400(location_dao):
    ex_location = location_dao.find_by_id(1400)
    ex_location['street_address'] = "123 Test St."

    affected = location_dao.update(ex_location)
    assert affected == 1

    location = location_dao.find_by_id(1400)
    assert location['location_id'] == 1400
    assert location['street_address'] == "123 Test St."
    assert location['postal_code'] == "26192"
    assert location['city'] == "Southlake"
    assert location['state_province'] == "Texas"
    assert location['country']['country_id'] == "US"


def test_update_where_id_is_1234(location_dao):
    ex_location = location_dao.find_by_id(1400)
    ex_location['location_id'] = 1234
    ex_location['street_address'] = "123 Test St."

    affected = location_dao.update(ex_location)
    assert affected == 0


def test_delete_where_id_is_1400(location_dao):
    ex_location = Location()
    ex_location['location_id'] = 1400

    affected = location_dao.delete(ex_location)
    assert affected == 1

    location = location_dao.find_by_id(1400)
    assert location is None

    locations = location_dao.find_all()
    assert len(locations) == 6


def test_delete_where_id_is_1234(location_dao):
    ex_location = Location()
    ex_location['location_id'] = 1234

    affected = location_dao.delete(ex_location)
    assert affected == 0

    locations = location_dao.find_all()
    assert len(locations) == 7
