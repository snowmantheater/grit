from datetime import date
import pytest
from .init_db import engine
from hr.dao import EmployeeDao, JobDao, DepartmentDao, DependentDao
from hr.entity import Dependent, Employee


@pytest.fixture
def employee_dao(engine):
    return EmployeeDao(engine)


@pytest.fixture
def job_dao(engine):
    return JobDao(engine)


@pytest.fixture
def department_dao(engine):
    return DepartmentDao(engine)


@pytest.fixture
def dependent_dao(engine):
    return DependentDao(engine)


# Update Existing Employee with No Dependents
def test_update_where_id_is_120(employee_dao, dependent_dao):
    ex_employee = employee_dao.find_by_id(120)
    assert len(ex_employee['dependents']) == 0
    ex_employee['first_name'] = "Test"

    affected = employee_dao.update(ex_employee)
    assert affected == (1, 0)

    employee = employee_dao.find_by_id(120)
    assert employee['employee_id'] == 120
    assert employee['first_name'] == "Test"
    assert len(employee['dependents']) == 0

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


# Update Existing Employee with Previous Dependent
def test_update_where_id_is_101(employee_dao, dependent_dao):
    ex_employee = employee_dao.find_by_id(101, expand=True)
    assert len(ex_employee['dependents']) == 1
    ex_employee['first_name'] = "Test"

    affected = employee_dao.update(ex_employee, expand=True)
    assert affected == (1, 1)

    employee = employee_dao.find_by_id(101, expand=True)
    assert employee['employee_id'] == 101
    assert employee['first_name'] == "Test"
    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5
    assert employee['dependents'][0]['first_name'] == "Johnny"
    assert employee['dependents'][0]['last_name'] == "Kochhar"
    assert employee['dependents'][0]['relationship'] == "Child"
    assert employee['dependents'][0]['employee_id'] == 101

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


# Update Existing Employee's Previous Dependent
def test_update_dependent_where_id_is_101(employee_dao, dependent_dao):
    ex_employee = employee_dao.find_by_id(101, expand=True)
    assert len(ex_employee['dependents']) == 1
    ex_employee['dependents'][0]['first_name'] = "Test"

    affected = employee_dao.update(ex_employee, expand=True)
    assert affected == (1, 1)

    employee = employee_dao.find_by_id(101, expand=True)
    assert employee['employee_id'] == 101
    assert employee['first_name'] == "Neena"
    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 5
    assert employee['dependents'][0]['first_name'] == "Test"

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_update_where_id_is_120_add_new_dependent(employee_dao, dependent_dao):
    new_dependent = Dependent()
    new_dependent['first_name'] = "John"
    new_dependent['last_name'] = "Doe"
    new_dependent['relationship'] = "Spouse"

    ex_employee = employee_dao.find_by_id(120, expand=True)
    assert len(ex_employee['dependents']) == 0
    ex_employee['dependents'].append(new_dependent)

    affected = employee_dao.update(ex_employee, expand=True)
    assert affected == (1, 1)

    employee = employee_dao.find_by_id(120, expand=True)
    assert employee['employee_id'] == 120
    assert employee['first_name'] == "Matthew"
    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 31
    assert employee['dependents'][0]['first_name'] == "John"
    assert employee['dependents'][0]['last_name'] == "Doe"
    assert employee['dependents'][0]['relationship'] == "Spouse"
    assert employee['dependents'][0]['employee_id'] == 120

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 31


# Update Existing Exmployee Adding Existing Dependent
def test_update_where_id_is_120_add_ex_dependent(employee_dao, dependent_dao):
    old_employee = employee_dao.find_by_id(206, expand=True)
    assert len(old_employee['dependents']) == 1

    ex_dependent = dependent_dao.find_by_id(1)
    ex_employee = employee_dao.find_by_id(120, expand=True)
    assert len(ex_employee['dependents']) == 0
    ex_employee['dependents'].append(ex_dependent)

    affected = employee_dao.update(ex_employee, expand=True)
    assert affected == (1, 1)

    old_employee = employee_dao.find_by_id(206, expand=True)
    assert len(old_employee['dependents']) == 0

    employee = employee_dao.find_by_id(120, expand=True)
    assert employee['employee_id'] == 120
    assert employee['first_name'] == "Matthew"
    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 1
    assert employee['dependents'][0]['first_name'] == "Penelope"
    assert employee['dependents'][0]['last_name'] == "Gietz"
    assert employee['dependents'][0]['relationship'] == "Child"
    assert employee['dependents'][0]['employee_id'] == 120

    employees = employee_dao.find_all()
    assert len(employees) == 40

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


# Update Non-Existing Employee
def test_update_where_id_is_999(employee_dao):
    ex_employee = employee_dao.find_by_id(120)
    ex_employee['employee_id'] = 999
    ex_employee['first_name'] = "Test"

    affected = employee_dao.update(ex_employee)
    assert affected == (0, 0)


# Update Non-Existing Employee (Expand)
def test_update_where_id_is_999_and_expand(employee_dao):
    ex_employee = employee_dao.find_by_id(120)
    ex_employee['employee_id'] = 999
    ex_employee['first_name'] = "Test"

    affected = employee_dao.update(ex_employee, expand=True)
    assert affected == (0, 0)
