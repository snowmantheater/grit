import pytest
from .init_db import engine
from hr.dao import RegionDao
from hr.entity import Region


@pytest.fixture
def region_dao(engine):
    return RegionDao(engine)


def test_find_all(region_dao):
    regions = region_dao.find_all()
    assert len(regions) == 4


def test_find_by_id_where_id_is_1(region_dao):
    region = region_dao.find_by_id(1)
    assert region['region_id'] == 1
    assert region['region_name'] == "Europe"


def test_find_by_id_where_id_is_5(region_dao):
    region = region_dao.find_by_id(5)
    assert region is None


def test_create(region_dao):
    new_region = Region()
    new_region['region_name'] = "Test"
    assert new_region['region_id'] is None

    region_dao.create(new_region)
    assert new_region['region_id'] == 5

    region = region_dao.find_by_id(5)
    assert region['region_id'] == 5
    assert region['region_name'] == "Test"


def test_update_where_id_is_1(region_dao):
    ex_region = region_dao.find_by_id(1)
    ex_region['region_name'] = "Test"

    affected = region_dao.update(ex_region)
    assert affected == 1

    region = region_dao.find_by_id(1)
    assert region['region_id'] == 1
    assert region['region_name'] == "Test"


def test_update_where_id_is_5(region_dao):
    ex_region = region_dao.find_by_id(1)
    ex_region['region_id'] = 5
    ex_region['region_name'] = "Test"

    affected = region_dao.update(ex_region)
    assert affected == 0


def test_delete_where_id_is_1(region_dao):
    ex_region = Region()
    ex_region['region_id'] = 1

    affected = region_dao.delete(ex_region)
    assert affected == 1

    region = region_dao.find_by_id(1)
    assert region is None

    regions = region_dao.find_all()
    assert len(regions) == 3


def test_delete_where_id_is_5(region_dao):
    ex_region = Region()
    ex_region['region_id'] = 5

    affected = region_dao.delete(ex_region)
    assert affected == 0

    regions = region_dao.find_all()
    assert len(regions) == 4
