from datetime import date
import pytest
from .init_db import engine
from hr.dao import EmployeeDao, JobDao, DepartmentDao, DependentDao
from hr.entity import Dependent, Employee


@pytest.fixture
def employee_dao(engine):
    return EmployeeDao(engine)


@pytest.fixture
def job_dao(engine):
    return JobDao(engine)


@pytest.fixture
def department_dao(engine):
    return DepartmentDao(engine)


@pytest.fixture
def dependent_dao(engine):
    return DependentDao(engine)


def test_create_with_no_dependents(employee_dao, department_dao, job_dao, dependent_dao):
    department = department_dao.find_by_id(2)
    job = job_dao.find_by_id(10)
    manager = employee_dao.find_by_id(117)
    new_employee = Employee()
    new_employee['first_name'] = "John"
    new_employee['last_name'] = "Doe"
    new_employee['email'] = "john.doe@email.com"
    new_employee['phone_number'] = "515.987.6543"
    new_employee['hire_date'] = date(2001, 11, 21)
    new_employee['salary'] = 40000
    new_employee['department'] = department
    new_employee['job'] = job
    new_employee['manager'] = manager

    employee_dao.create(new_employee)
    assert new_employee['employee_id'] == 207

    employees = employee_dao.find_all()
    assert len(employees) == 41

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30

    employee = employee_dao.find_by_id(207)
    assert employee['first_name'] == "John"
    assert employee['last_name'] == "Doe"
    assert employee['email'] == "john.doe@email.com"
    assert employee['phone_number'] == "515.987.6543"
    assert employee['hire_date'] == date(2001, 11, 21)
    assert employee['salary'] == 40000
    assert employee['department']['department_id'] == 2
    assert employee['job']['job_id'] == 10
    assert employee['manager']['employee_id'] == 117
    assert len(employee['dependents']) == 0


def test_create_with_new_dependent(employee_dao, department_dao, job_dao, dependent_dao):
    department = department_dao.find_by_id(2)
    job = job_dao.find_by_id(10)
    manager = employee_dao.find_by_id(117)
    new_employee = Employee()
    new_employee['first_name'] = "John"
    new_employee['last_name'] = "Doe"
    new_employee['email'] = "john.doe@email.com"
    new_employee['phone_number'] = "515.987.6543"
    new_employee['hire_date'] = date(2001, 11, 21)
    new_employee['salary'] = 40000
    new_employee['department'] = department
    new_employee['job'] = job
    new_employee['manager'] = manager

    new_dependent = Dependent()
    new_dependent['first_name'] = "Jane"
    new_dependent['last_name'] = "Doe"
    new_dependent['relationship'] = "Spouse"

    new_employee['dependents'].append(new_dependent)

    employee_dao.create(new_employee, expand=True)
    assert new_employee['employee_id'] == 207
    assert new_dependent['dependent_id'] == 31
    assert new_dependent['employee_id'] == 207

    employees = employee_dao.find_all()
    assert len(employees) == 41

    dependents = dependent_dao.find_all()
    assert len(dependents) == 31

    employee = employee_dao.find_by_id(207, expand=True)
    assert employee['first_name'] == "John"
    assert employee['last_name'] == "Doe"
    assert employee['email'] == "john.doe@email.com"
    assert employee['phone_number'] == "515.987.6543"
    assert employee['hire_date'] == date(2001, 11, 21)
    assert employee['salary'] == 40000
    assert employee['department']['department_id'] == 2
    assert employee['job']['job_id'] == 10
    assert employee['manager']['employee_id'] == 117

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 31
    assert employee['dependents'][0]['first_name'] == "Jane"
    assert employee['dependents'][0]['last_name'] == "Doe"
    assert employee['dependents'][0]['relationship'] == "Spouse"
    assert employee['dependents'][0]['employee_id'] == 207


def test_create_with_ex_dependent(employee_dao, department_dao, job_dao, dependent_dao):
    department = department_dao.find_by_id(2)
    job = job_dao.find_by_id(10)
    manager = employee_dao.find_by_id(117)
    new_employee = Employee()
    new_employee['first_name'] = "John"
    new_employee['last_name'] = "Doe"
    new_employee['email'] = "john.doe@email.com"
    new_employee['phone_number'] = "515.987.6543"
    new_employee['hire_date'] = date(2001, 11, 21)
    new_employee['salary'] = 40000
    new_employee['department'] = department
    new_employee['job'] = job
    new_employee['manager'] = manager

    ex_dependent = dependent_dao.find_by_id(1)

    new_employee['dependents'].append(ex_dependent)

    employee_dao.create(new_employee, expand=True)
    assert new_employee['employee_id'] == 207
    assert ex_dependent['dependent_id'] == 1
    assert ex_dependent['employee_id'] == 207

    employees = employee_dao.find_all()
    assert len(employees) == 41

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30

    employee = employee_dao.find_by_id(207, expand=True)
    assert employee['first_name'] == "John"
    assert employee['last_name'] == "Doe"
    assert employee['email'] == "john.doe@email.com"
    assert employee['phone_number'] == "515.987.6543"
    assert employee['hire_date'] == date(2001, 11, 21)
    assert employee['salary'] == 40000
    assert employee['department']['department_id'] == 2
    assert employee['job']['job_id'] == 10
    assert employee['manager']['employee_id'] == 117

    assert len(employee['dependents']) == 1
    assert employee['dependents'][0]['dependent_id'] == 1
    assert employee['dependents'][0]['first_name'] == "Penelope"
    assert employee['dependents'][0]['last_name'] == "Gietz"
    assert employee['dependents'][0]['relationship'] == "Child"
    assert employee['dependents'][0]['employee_id'] == 207
