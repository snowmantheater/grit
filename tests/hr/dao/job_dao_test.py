import pytest
from .init_db import engine
from hr.dao import JobDao
from hr.entity import Job


@pytest.fixture
def job_dao(engine):
    return JobDao(engine)


def test_find_all(job_dao):
    jobs = job_dao.find_all()
    assert len(jobs) == 19


def test_find_by_id_where_id_is_9(job_dao):
    job = job_dao.find_by_id(9)
    assert job['job_id'] == 9
    assert job['job_title'] == "Programmer"
    assert job['min_salary'] == 4000
    assert job['max_salary'] == 10000


def test_find_by_id_where_id_is_20(job_dao):
    job = job_dao.find_by_id(20)
    assert job is None


def test_create(job_dao):
    new_job = Job()
    new_job['job_title'] = "Test Job"
    new_job['min_salary'] = 8000
    new_job['max_salary'] = 12000

    job_dao.create(new_job)
    assert new_job['job_id'] == 20

    job = job_dao.find_by_id(20)
    assert job['job_id'] == 20
    assert job['job_title'] == "Test Job"
    assert job['min_salary'] == 8000
    assert job['max_salary'] == 12000


def test_update_where_id_is_9(job_dao):
    ex_job = job_dao.find_by_id(9)
    ex_job['job_title'] = "Test Job"

    affected = job_dao.update(ex_job)
    assert affected == 1

    job = job_dao.find_by_id(9)
    assert job['job_id'] == 9
    assert job['job_title'] == "Test Job"
    assert job['min_salary'] == 4000
    assert job['max_salary'] == 10000


def test_update_where_id_is_20(job_dao):
    ex_job = job_dao.find_by_id(9)
    ex_job['job_id'] = 20
    ex_job['job_title'] = "Test Job"

    affected = job_dao.update(ex_job)
    assert affected == 0


def test_delete_where_id_is_9(job_dao):
    ex_job = Job()
    ex_job['job_id'] = 9

    affected = job_dao.delete(ex_job)
    assert affected == 1

    job = job_dao.find_by_id(9)
    assert job is None

    jobs = job_dao.find_all()
    assert len(jobs) == 18


def test_delete_where_id_is_20(job_dao):
    ex_job = Job()
    ex_job['job_id'] = 20

    affected = job_dao.delete(ex_job)
    assert affected == 0

    jobs = job_dao.find_all()
    assert len(jobs) == 19
