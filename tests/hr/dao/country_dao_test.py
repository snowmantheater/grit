import pytest
from .init_db import engine
from hr.dao import CountryDao, RegionDao
from hr.entity import Country


@pytest.fixture
def country_dao(engine):
    return CountryDao(engine)


@pytest.fixture
def region_dao(engine):
    return RegionDao(engine)


def test_find_all(country_dao):
    countries = country_dao.find_all()
    assert len(countries) == 25


def test_find_by_id_where_id_is_ca(country_dao):
    country = country_dao.find_by_id('CA')
    assert country['country_id'] == "CA"
    assert country['country_name'] == "Canada"
    assert country['region']['region_id'] == 2
    assert country['region']['region_name'] is None


def test_find_by_id_where_id_is_ca_and_expand(country_dao):
    country = country_dao.find_by_id('CA', expand=True)
    assert country['country_id'] == "CA"
    assert country['country_name'] == "Canada"
    assert country['region']['region_id'] == 2
    assert country['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_ca_and_depth_is_1(country_dao):
    country = country_dao.find_by_id('CA', depth=1)
    assert country['country_id'] == "CA"
    assert country['country_name'] == "Canada"
    assert country['region']['region_id'] == 2
    assert country['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_xx(country_dao):
    country = country_dao.find_by_id('XX')
    assert country is None

    country = country_dao.find_by_id('XX', expand=True)
    assert country is None


def test_create(country_dao, region_dao):
    region = region_dao.find_by_id(1)
    new_country = Country()
    new_country['country_id'] = "XX"
    new_country['country_name'] = "Test"
    new_country['region'] = region

    country_dao.create(new_country)
    assert new_country['country_id'] == "XX"

    country = country_dao.find_by_id('XX')
    assert country['country_id'] == "XX"
    assert country['country_name'] == "Test"
    assert country['region']['region_id'] == 1


def test_update_where_id_is_ca(country_dao):
    ex_country = country_dao.find_by_id('CA')
    ex_country['country_name'] = "Test"

    affected = country_dao.update(ex_country)
    assert affected == 1

    country = country_dao.find_by_id('CA')
    assert country['country_id'] == "CA"
    assert country['country_name'] == "Test"
    assert country['region']['region_id'] == 2


def test_update_where_id_is_xx(country_dao):
    ex_country = country_dao.find_by_id('CA')
    ex_country['country_id'] = "XX"
    ex_country['country_name'] = "Test"

    affected = country_dao.update(ex_country)
    assert affected == 0


def test_delete_where_id_is_ca(country_dao):
    ex_country = Country()
    ex_country['country_id'] = "CA"

    affected = country_dao.delete(ex_country)
    assert affected == 1

    country = country_dao.find_by_id('CA')
    assert country is None

    countries = country_dao.find_all()
    assert len(countries) == 24


def test_delete_where_id_is_5(country_dao):
    ex_country = Country()
    ex_country['country_id'] = "XX"

    affected = country_dao.delete(ex_country)
    assert affected == 0

    countries = country_dao.find_all()
    assert len(countries) == 25
