import pytest
from .init_db import engine
from hr.dao import DependentDao
from hr.entity import Dependent


@pytest.fixture
def dependent_dao(engine):
    return DependentDao(engine)


def test_find_all(dependent_dao):
    dependents = dependent_dao.find_all()
    assert len(dependents) == 30


def test_find_by_id_where_id_is_1(dependent_dao):
    dependent = dependent_dao.find_by_id(1)
    assert dependent['dependent_id'] == 1
    assert dependent['first_name'] == "Penelope"
    assert dependent['last_name'] == "Gietz"
    assert dependent['relationship'] == "Child"
    assert dependent['employee_id'] == 206


def test_find_by_id_where_id_is_31(dependent_dao):
    dependent = dependent_dao.find_by_id(31)
    assert dependent is None


def test_find_by_employee_id_where_employee_id_is_206(dependent_dao):
    dependents = dependent_dao.find_by_employee_id(206)
    assert len(dependents) == 1
    assert dependents[0]['dependent_id'] == 1
    assert dependents[0]['first_name'] == "Penelope"
    assert dependents[0]['last_name'] == "Gietz"
    assert dependents[0]['relationship'] == "Child"
    assert dependents[0]['employee_id'] == 206


def test_find_by_employee_where_employee_id_is_99(dependent_dao):
    dependents = dependent_dao.find_by_employee_id(99)
    assert len(dependents) == 0


def test_create(dependent_dao):
    new_dependent = Dependent()
    new_dependent['first_name'] = "John"
    new_dependent['last_name'] = "Doe"
    new_dependent['relationship'] = "Parent"
    new_dependent['employee_id'] = 0

    dependent_dao.create(new_dependent)
    assert new_dependent['dependent_id'] == 31

    dependent = dependent_dao.find_by_id(31)
    assert dependent['first_name'] == "John"
    assert dependent['last_name'] == "Doe"
    assert dependent['relationship'] == "Parent"
    assert dependent['employee_id'] == 0


def test_update_where_id_is_1(dependent_dao):
    ex_dependent = dependent_dao.find_by_id(1)
    ex_dependent['last_name'] = "Test"

    affected = dependent_dao.update(ex_dependent)
    assert affected == 1

    dependent = dependent_dao.find_by_id(1)
    assert dependent['dependent_id'] == 1
    assert dependent['first_name'] == "Penelope"
    assert dependent['last_name'] == "Test"
    assert dependent['relationship'] == "Child"
    assert dependent['employee_id'] == 206


def test_update_where_id_is_31(dependent_dao):
    ex_dependent = dependent_dao.find_by_id(1)
    ex_dependent['dependent_id'] = 99
    ex_dependent['last_name'] = "Test"

    affected = dependent_dao.update(ex_dependent)
    assert affected == 0


def test_delete_where_id_is_1(dependent_dao):
    ex_dependent = Dependent()
    ex_dependent['dependent_id'] = 1

    affected = dependent_dao.delete(ex_dependent)
    assert affected == 1

    dependent = dependent_dao.find_by_id(1)
    assert dependent is None

    dependents = dependent_dao.find_all()
    assert len(dependents) == 29


def test_delete_where_id_is_31(dependent_dao):
    ex_dependent = Dependent()
    ex_dependent['dependent_id'] = 31

    affected = dependent_dao.delete(ex_dependent)
    assert affected == 0

    dependents = dependent_dao.find_all()
    assert len(dependents) == 30
