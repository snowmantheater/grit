import pytest
from .init_db import engine
from hr.dao import DepartmentDao, LocationDao
from hr.entity import Department


@pytest.fixture
def department_dao(engine):
    return DepartmentDao(engine)


@pytest.fixture
def location_dao(engine):
    return LocationDao(engine)


def test_find_all(department_dao):
    departments = department_dao.find_all()
    assert len(departments) == 11


def test_find_by_id_where_id_is_6(department_dao):
    department = department_dao.find_by_id(6)
    assert department['department_id'] == 6
    assert department['department_name'] == "IT"
    assert department['location']['location_id'] == 1400
    assert department['location']['street_address'] is None
    assert department['location']['postal_code'] is None
    assert department['location']['city'] is None
    assert department['location']['state_province'] is None
    assert department['location']['country'] is None


def test_find_by_id_where_id_is_6_and_expand(department_dao):
    department = department_dao.find_by_id(6, expand=True)
    assert department['department_id'] == 6
    assert department['department_name'] == "IT"
    assert department['location']['location_id'] == 1400
    assert department['location']['street_address'] == "2014 Jabberwocky Rd"
    assert department['location']['postal_code'] == "26192"
    assert department['location']['city'] == "Southlake"
    assert department['location']['state_province'] == "Texas"
    assert department['location']['country']['country_id'] == "US"
    assert department['location']['country']['country_name'] == "United States of America"
    assert department['location']['country']['region']['region_id'] == 2
    assert department['location']['country']['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_6_and_depth_is_1(department_dao):
    department = department_dao.find_by_id(6, depth=1)
    assert department['department_id'] == 6
    assert department['department_name'] == "IT"
    assert department['location']['location_id'] == 1400
    assert department['location']['country']['country_id'] == "US"
    assert department['location']['country']['country_name'] is None
    assert department['location']['country']['region'] is None


def test_find_by_id_where_id_is_6_and_depth_is_2(department_dao):
    department = department_dao.find_by_id(6, depth=2)
    assert department['department_id'] == 6
    assert department['department_name'] == "IT"
    assert department['location']['location_id'] == 1400
    assert department['location']['country']['region']['region_id'] == 2
    assert department['location']['country']['region']['region_name'] is None


def test_find_by_id_where_id_is_6_and_depth_is_3(department_dao):
    department = department_dao.find_by_id(6, depth=3)
    assert department['department_id'] == 6
    assert department['department_name'] == "IT"
    assert department['location']['location_id'] == 1400
    assert department['location']['country']['region']['region_name'] == "Americas"


def test_find_by_id_where_id_is_12(department_dao):
    department = department_dao.find_by_id(12)
    assert department is None

    department = department_dao.find_by_id(12, expand=True)
    assert department is None


def test_create(department_dao, location_dao):
    location = location_dao.find_by_id(1400)
    new_department = Department()
    new_department['department_name'] = "Test"
    new_department['location'] = location

    department_dao.create(new_department)
    assert new_department['department_id'] == 12

    department = department_dao.find_by_id(12)
    assert department['department_id'] == 12
    assert department['department_name'] == "Test"
    assert department['location']['location_id'] == 1400


def test_update_where_id_is_6(department_dao):
    ex_department = department_dao.find_by_id(6)
    ex_department['department_name'] = "Test"

    affected = department_dao.update(ex_department)
    assert affected == 1

    department = department_dao.find_by_id(6)
    assert department['department_id'] == 6
    assert department['department_name'] == "Test"
    assert department['location']['location_id'] == 1400


def test_update_where_id_is_12(department_dao):
    ex_department = department_dao.find_by_id(6)
    ex_department['department_id'] = 12
    ex_department['department_name'] = "Test"

    affected = department_dao.update(ex_department)
    assert affected == 0


def test_delete_where_id_is_6(department_dao):
    ex_department = Department()
    ex_department['department_id'] = 6

    affected = department_dao.delete(ex_department)
    assert affected == 1

    department = department_dao.find_by_id(6)
    assert department is None

    departments = department_dao.find_all()
    assert len(departments) == 10


def test_delete_where_id_is_12(department_dao):
    ex_department = Department()
    ex_department['department_id'] = 12

    affected = department_dao.delete(ex_department)
    assert affected == 0

    departments = department_dao.find_all()
    assert len(departments) == 11
