import pytest
from gritdb import Plural, Singular


def test_plural_new_where_keys_is_empty():
    with pytest.raises(ValueError):
        Plural()


def test_plural_new_where_keys_is_str():
    p = Plural("aaa")
    assert len(p.keys) == 1
    assert ("aaa", "aaa") in p.keys


def test_plural_new_where_keys_is_str_str():
    p = Plural(("aaa", "bbb"))
    assert len(p.keys) == 1
    assert ("aaa", "bbb") in p.keys


def test_plural_new_where_keys_is_2str():
    p = Plural("aaa", "bbb")
    assert len(p.keys) == 2
    assert ("aaa", "aaa") in p.keys
    assert ("bbb", "bbb") in p.keys


def test_plural_new_where_keys_is_2str_str():
    p = Plural(("aaa", "bbb"), ("ccc", "ddd"))
    assert len(p.keys) == 2
    assert ("aaa", "bbb") in p.keys
    assert ("ccc", "ddd") in p.keys


def test_singular_new_where_keys_is_empty():
    with pytest.raises(ValueError):
        Singular()


def test_singular_new_where_keys_is_str():
    s = Singular("aaa")
    assert len(s.keys) == 1
    assert ("aaa", "aaa") in s.keys


def test_singular_new_where_keys_is_str_str():
    s = Singular(("aaa", "bbb"))
    assert len(s.keys) == 1
    assert ("aaa", "bbb") in s.keys


def test_singular_new_where_keys_is_2str():
    s = Singular("aaa", "bbb")
    assert len(s.keys) == 2
    assert ("aaa", "aaa") in s.keys
    assert ("bbb", "bbb") in s.keys


def test_singular_new_where_keys_is_2str_str():
    s = Singular(("aaa", "bbb"), ("ccc", "ddd"))
    assert len(s.keys) == 2
    assert ("aaa", "bbb") in s.keys
    assert ("ccc", "ddd") in s.keys
