import pytest
from gritdb import Entity, Plural


class Alpha(Entity):
    def __init__(self):
        super().__init__("alpha")
        self.column("alpha_id", key=True)
        self.column("parent_id")
        self.column("afield", required=True)


class Bravo(Entity):
    def __init__(self):
        super().__init__("bravo")
        self.column("bravo_id", key=True)
        self.column("bfield")
        self.column("alphas", related=Plural(("parent_id", "bravo_id")))


@pytest.fixture(scope="function")
def alpha_collection():
    alpha1 = Alpha()
    alpha1['alpha_id'] = "aid1"
    alpha1['parent_id'] = "x"
    alpha1['afield'] = "alphafield1"

    alpha2 = Alpha()
    alpha2['alpha_id'] = "aid2"
    alpha2['parent_id'] = "y"
    alpha2['afield'] = "alphafield2"

    alpha3 = Alpha()
    alpha3['alpha_id'] = "aid3"
    alpha3['parent_id'] = "z"
    alpha3['afield'] = "alphafield3"

    return [alpha1, alpha2, alpha3]


@pytest.fixture(scope="function")
def xbravo():
    return Bravo()


@pytest.fixture(scope="function")
def bravo(alpha_collection):
    bravo = Bravo()
    bravo['bravo_id'] = "bid"
    bravo['bfield'] = "bravofield"
    bravo['alphas'] = alpha_collection
    return bravo


def test_align_where_instance_is_populated(bravo):
    bravo.reparent('alphas')
    for alpha in bravo['alphas']:
        assert alpha['parent_id'] == "bid"


def test_set_where_value_is_not_list(xbravo):
    with pytest.raises(ValueError):
        xbravo['alphas'] = "error"


def test_params_where_instance_is_empty_and_fields_is_true(xbravo):
    p = xbravo.params(fields=True)
    assert len(p) == 1
    assert 'bfield' in p
    assert p['bfield'] is None
    assert 'alphas' not in p


def test_params_where_instance_is_populated_and_fields_is_true(bravo):
    p = bravo.params(fields=True)
    assert len(p) == 1
    assert 'bfield' in p
    assert p['bfield'] == "bravofield"
    assert 'alphas' not in p
