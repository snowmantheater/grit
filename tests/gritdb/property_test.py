import pytest
from gritdb import Property


def test_new_where_name_is_str():
    p = Property("aaa")
    assert p.name == "aaa"
    assert p.value is None


def test_new_where_name_is_str_and_value_is_none():
    p = Property("aaa", None)
    assert p.name == "aaa"
    assert p.value is None


def test_new_where_name_is_str_and_value_is_str():
    p = Property("aaa", "bbb")
    assert p.name == "aaa"
    assert p.value == "bbb"


def test_new_where_name_is_str_and_value_is_int():
    p = Property("aaa", 1)
    assert p.name == "aaa"
    assert p.value == 1


def test_new_where_name_is_none():
    with pytest.raises(ValueError):
        Property(None)


def test_set_where_value_is_str():
    p = Property("aaa")
    p.value = "bbb"
    assert p.value == "bbb"


def test_set_where_value_is_int():
    p = Property("aaa")
    p.value = 1
    assert p.value == 1
