import pytest
from decimal import Decimal
from gritdb import Column, Plural, Singular

# =====================================================================================================[INSTANTIATION]==


def test_new():
    c = Column("aaa")
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is False
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None


def test_new_where_required_is_true():
    c = Column("aaa", required=True)
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is True
    assert c.iskey is False
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None


def test_new_where_key_is_true():
    c = Column("aaa", key=True)
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is True
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None


def test_new_where_required_is_true_and_key_is_true():
    c = Column("aaa", required=True, key=True)
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is True
    assert c.iskey is True
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None

# --------------------------------------------------------------------------------------------------------------[size]--


def test_new_where_size_is_int():
    c = Column("aaa", size=1)
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is False
    assert c.size == 1
    assert c.size_d is None
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None


def test_new_where_size_is_t2int():
    c = Column("aaa", size=(1, 2))
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is False
    assert c.size == 1
    assert c.size_d == 2
    assert c.isrelated is False
    assert c.isplural is False
    assert c.issingular is False
    assert c.foreign_keys is None


def test_new_where_size_is_t3int():
    with pytest.raises(ValueError):
        Column("aaa", size=(1, 2, 3))


def test_new_where_size_is_str():
    with pytest.raises(ValueError):
        Column("aaa", size="zzz")

# -----------------------------------------------------------------------------------------------------------[related]--


def test_new_where_related_is_plural():
    c = Column("aaa", related=Plural("key"))
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is False
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is True
    assert c.isplural is True
    assert c.issingular is False
    assert c.foreign_keys == (("key", "key"),)


def test_new_where_key_is_true_and_related_is_plural():
    with pytest.raises(ValueError):
        Column("aaa", key=True, related=Plural("key"))


def test_new_where_related_is_singular():
    c = Column("aaa", related=Singular("key"))
    assert c.name == "aaa"
    assert c.value is None
    assert c.isrequired is False
    assert c.iskey is False
    assert c.size is None
    assert c.size_d is None
    assert c.isrelated is True
    assert c.isplural is False
    assert c.issingular is True
    assert c.foreign_keys == (("key", "key"),)


def test_new_where_key_is_true_and_related_is_singular():
    with pytest.raises(ValueError):
        Column("aaa", key=True, related=Singular("key"))

# ========================================================================================================[VALIDATION]==


def test_validate_create_delete():
    c = Column("aaa")
    with pytest.raises(ValueError):
        c.validate(create=True, delete=True)

# -----------------------------------------------------------------------------------------------------[required=True]--


def test_validate_where_value_is_none_and_required_is_true():
    c = Column("aaa", required=True)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_create_where_value_is_none_and_required_is_true():
    c = Column("aaa", required=True)
    with pytest.raises(ValueError):
        c.validate(create=True)


def test_validate_delete_where_value_is_none_and_required_is_true():
    c = Column("aaa", required=True)
    c.validate(delete=True)


def test_validate_where_value_is_str_and_required_is_true():
    c = Column("aaa", "bbb", required=True)
    c.validate()


def test_validate_create_where_value_is_str_and_required_is_true():
    c = Column("aaa", "bbb", required=True)
    c.validate(create=True)


def test_validate_delete_where_value_is_str_and_required_is_true():
    c = Column("aaa", "bbb", required=True)
    c.validate(delete=True)

# ----------------------------------------------------------------------------------------------------------[key=True]--


def test_validate_where_value_is_none_and_key_is_true():
    c = Column("aaa", key=True)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_create_where_value_is_none_and_key_is_true():
    c = Column("aaa", key=True)
    c.validate(create=True)


def test_validate_delete_where_value_is_none_and_key_is_true():
    c = Column("aaa", key=True)
    with pytest.raises(ValueError):
        c.validate(delete=True)


def test_validate_where_value_is_str_and_key_is_true():
    c = Column("aaa", "bbb", key=True)
    c.validate()


def test_validate_create_where_value_is_str_and_key_is_true():
    c = Column("aaa", "bbb", key=True)
    c.validate(create=True)


def test_validate_delete_where_value_is_str_and_key_is_true():
    c = Column("aaa", "bbb", key=True)
    c.validate(delete=True)

# -------------------------------------------------------------------------------------------[required=True, key=True]--


def test_validate_where_value_is_none_and_required_is_true_key_is_true():
    c = Column("aaa", required=True, key=True)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_create_where_value_is_none_and_required_is_true_key_is_true():
    c = Column("aaa", required=True, key=True)
    with pytest.raises(ValueError):
        c.validate(create=True)


def test_validate_delete_where_value_is_none_and_required_is_true_key_is_true():
    c = Column("aaa", required=True, key=True)
    with pytest.raises(ValueError):
        c.validate(delete=True)


def test_validate_where_value_is_str_and_required_is_true_key_is_true():
    c = Column("aaa", "bbb", required=True, key=True)
    c.validate()


def test_validate_create_where_value_is_str_and_required_is_true_key_is_true():
    c = Column("aaa", "bbb", required=True, key=True)
    c.validate(create=True)


def test_validate_delete_where_value_is_str_and_required_is_true_key_is_true():
    c = Column("aaa", "bbb", required=True, key=True)
    c.validate(delete=True)

# ============================================================================================================[length]==

# --------------------------------------------------------------------------------------------------------[value=None]--


def test_validate_where_value_is_none_and_size_is_5():
    c = Column("aaa", size=5)
    c.validate()


def test_validate_where_value_is_none_and_size_is_5_2():
    c = Column("aaa", size=(5, 2))
    c.validate()

# ---------------------------------------------------------------------------------------------------------[value=str]--


def test_validate_where_value_is_str_and_size_is_5():
    c = Column("aaa", "123", size=5)
    c.validate()

    c = Column("aaa", "123456", size=5)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_str_no_dot_and_size_is_5_2():
    c = Column("aaa", "123", size=(5, 2))
    c.validate()

    c = Column("aaa", "123456", size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_str_1dot_and_size_is_5_2():
    c = Column("aaa", "123.1", size=(5, 2))
    c.validate()

    c = Column("aaa", "123456.1", size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", "123.123", size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", "123456.123", size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_str_2dot_and_size_is_5_2():
    c = Column("aaa", "123.1.1", size=(5, 2))
    c.validate()

# ---------------------------------------------------------------------------------------------------------[value=int]--


def test_validate_where_value_is_int_and_size_is_5():
    c = Column("aaa", 123, size=5)
    c.validate()

    c = Column("aaa", 123456, size=5)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_int_and_size_is_5_2():
    c = Column("aaa", 123, size=(5, 2))
    c.validate()

    c = Column("aaa", 123456, size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

# -------------------------------------------------------------------------------------------------------[value=float]--


def test_validate_where_value_is_float_and_size_is_5():
    c = Column("aaa", 123.1, size=5)
    c.validate()

    c = Column("aaa", 1234.1, size=5)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_float_and_size_is_5_2():
    c = Column("aaa", 123.1, size=(5, 2))
    c.validate()

    c = Column("aaa", 123456.1, size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", 123.123, size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", 123456.123, size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

# -----------------------------------------------------------------------------------------------------[value=Decimal]--


def test_validate_where_value_is_Decimal_and_length_is_5():
    c = Column("aaa", Decimal("123.1"), size=5)
    c.validate()

    c = Column("aaa", Decimal("1234.1"), size=5)
    with pytest.raises(ValueError):
        c.validate()


def test_validate_where_value_is_Decimal_and_length_is_5_2():
    c = Column("aaa", Decimal("123.1"), size=(5, 2))
    c.validate()

    c = Column("aaa", Decimal("123456.1"), size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", Decimal("123.123"), size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()

    c = Column("aaa", Decimal("123456.123"), size=(5, 2))
    with pytest.raises(ValueError):
        c.validate()
