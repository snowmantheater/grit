import pytest
from gritdb import SqlCollection


def test_new():
    sc = SqlCollection()
    assert len(sc.collection) == 0


def test_parse_key_with_str():
    sc = SqlCollection()
    entity, criteria = sc._parse_key("entity")
    assert entity == "entity"
    assert criteria == ()


def test_parse_key_with_tuple_of_str():
    sc = SqlCollection()
    entity, criteria = sc._parse_key(("entity",))
    assert entity == "entity"
    assert criteria == ()


def test_parse_key_with_tuple_of_str_and_empty_tuple():
    sc = SqlCollection()
    entity, criteria = sc._parse_key(("entity", ()))
    assert entity == "entity"
    assert criteria == ()


def test_parse_key_with_tuple_of_str_and_tuple_of_str():
    sc = SqlCollection()
    entity, criteria = sc._parse_key(("entity", ("key1",)))
    assert entity == "entity"
    assert criteria == ("key1",)


def test_parse_key_with_tuple_of_str_and_tuple_of_str_str():
    sc = SqlCollection()
    entity, criteria = sc._parse_key(("entity", ("key1", "key2")))
    assert entity == "entity"
    assert criteria == ("key1", "key2")


def test_set_with_entity():
    sc = SqlCollection()
    sc['entity'] = "sql"
    assert len(sc.collection) == 1


def test_set_with_entity_criteria():
    sc = SqlCollection()
    sc['entity', "key"] = "sql"
    assert len(sc.collection) == 1


def test_set_with_entity_twice():
    sc = SqlCollection()
    sc['entity'] = "sql"
    with pytest.warns(RuntimeWarning):
        sc['entity'] = "sql"
    assert len(sc.collection) == 1


def test_set_with_entity_criteria_twice():
    sc = SqlCollection()
    sc['entity', ("key",)] = "sql"
    with pytest.warns(RuntimeWarning):
        sc['entity', "key"] = "sql"
    assert len(sc.collection) == 1


def test_get_with_entity():
    sc = SqlCollection()
    sc['entity'] = "sql"
    assert sc['entity'].text == "sql"


def test_get_with_missing_entity():
    sc = SqlCollection()
    with pytest.raises(IndexError):
        s = sc['entity']


def test_get_with_entity_criteria():
    sc = SqlCollection()
    sc['entity', "key"] = "sql"
    assert sc['entity', "key"].text == "sql"


def test_get_with_missing_entity_criteria():
    sc = SqlCollection()
    with pytest.raises(IndexError):
        s = sc['entity', ("key",)]
