import pytest
from gritdb import Entity, Singular


class Alpha(Entity):
    def __init__(self):
        super().__init__("alpha")
        self.column("alpha_id", key=True)
        self.column("afield", required=True)


class Bravo(Entity):
    def __init__(self):
        super().__init__("bravo")
        self.column("bravo_id", key=True)
        self.column("alpha", related=Singular(("alpha_id", "child_id")))


@pytest.fixture(scope="function")
def alpha():
    alpha = Alpha()
    alpha['alpha_id'] = "aid"
    alpha['afield'] = "alphafield"
    return alpha


@pytest.fixture(scope="function")
def xbravo():
    return Bravo()


@pytest.fixture(scope="function")
def bravo(alpha):
    bravo = Bravo()
    bravo['bravo_id'] = "bid"
    bravo['alpha'] = alpha
    return bravo


def test_params_where_instance_is_empty_and_fields_is_true(xbravo):
    p = xbravo.params(fields=True)
    assert len(p) == 1
    assert 'child_id' in p
    assert p['child_id'] is None


def test_params_where_instance_is_populated_and_fields_is_true(bravo):
    p = bravo.params(fields=True)
    assert len(p) == 1
    assert 'child_id' in p
    assert p['child_id'] == "aid"
