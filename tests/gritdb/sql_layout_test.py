from pathlib import Path
import pytest
from gritdb import SqlLayout


@pytest.fixture
def sql_layouts_path():
    return Path(__file__).parent / "sql_layouts"


def test_stmt_for_entity(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_sl_slt.sql")
    assert not sl.create.collection
    assert not sl.read.collection
    assert not sl.update.collection
    assert not sl.delete.collection
    assert sl.statement.collection


def test_stmt_for_entity_sl_slt(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_sl_slt.sql")
    assert sl.statement['entity'].text == "Single line of SQL, same line terminal"


def test_stmt_for_entity_sl_slt_ws(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_sl_slt_ws.sql")
    assert sl.statement['entity'].text == "Single line of SQL, same line terminal, with whitespace"


def test_stmt_for_entity_sl_nlt(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_sl_nlt.sql")
    assert sl.statement['entity'].text == "Single line of SQL, next line terminal"


def test_stmt_for_entity_sl_nlt_ws(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_sl_nlt_ws.sql")
    assert sl.statement['entity'].text == "Single line of SQL, next line terminal, with whitespace"


def test_stmt_for_entity_ml_slt(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_ml_slt.sql")
    assert sl.statement['entity'].text == "Multiple lines of SQL, same line terminal"


def test_stmt_for_entity_ml_slt_ws(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_ml_slt_ws.sql")
    assert sl.statement['entity'].text == "Multiple lines of SQL, same line terminal, with whitespace"


def test_stmt_for_entity_ml_nlt(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_ml_nlt.sql")
    assert sl.statement['entity'].text == "Multiple lines of SQL, next line terminal"


def test_stmt_for_entity_ml_nlt_ws(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity_ml_nlt_ws.sql")
    assert sl.statement['entity'].text == "Multiple lines of SQL, next line terminal, with whitespace"


def test_stmt_for_entity_criteria_1(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity-criteria-1.sql")
    assert sl.statement['entity', 'key1'].text == "Test SQL"


def test_stmt_for_entity_wwo_criteria_1(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity-wwo-criteria-1.sql")
    assert sl.statement['entity'].text == "Test SQL, no criteria"
    assert sl.statement['entity', 'key1'].text == "Test SQL, with criteria"


def test_stmt_for_entity_criteria_2(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity-criteria-2.sql")
    assert sl.statement['entity', ('key1', 'key2')].text == "Test SQL"


def test_stmt_for_entity_criteria_1_2(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_statement-entity-criteria-2_1.sql")
    assert sl.statement['entity', 'key1'].text == "Test SQL, with one criteria"
    assert sl.statement['entity', ('key1', 'key2')].text == "Test SQL, with two criteria"


def test_stmt_bad(sql_layouts_path):
    with pytest.raises(ValueError):
        sl = SqlLayout(sql_layouts_path / "test_statement-bad.sql")


def test_create_for_entity(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_create-entity.sql")
    assert sl.create.collection
    assert not sl.read.collection
    assert not sl.delete.collection
    assert not sl.update.collection
    assert not sl.statement.collection


def test_read_for_entity(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_read-entity.sql")
    assert not sl.create.collection
    assert sl.read.collection
    assert not sl.delete.collection
    assert not sl.update.collection
    assert not sl.statement.collection


def test_delete_for_entity(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_delete-entity.sql")
    assert not sl.create.collection
    assert not sl.read.collection
    assert sl.delete.collection
    assert not sl.update.collection
    assert not sl.statement.collection


def test_update_for_entity(sql_layouts_path):
    sl = SqlLayout(sql_layouts_path / "test_update-entity.sql")
    assert not sl.create.collection
    assert not sl.read.collection
    assert not sl.delete.collection
    assert sl.update.collection
    assert not sl.statement.collection
