import pytest
from gritdb import RowMapper


class Mapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self.last_row = None

    def maprow(self, row):
        self.last_row = row


def test_extend_no_overrides():
    class TestRowMapper(RowMapper):
        def __init__(self):
            super().__init__()

    with pytest.raises(TypeError):
        trm = TestRowMapper()


def test_extend_override():
    class TestRowMapper(RowMapper):
        def __init__(self):
            super().__init__()

        def maprow(self, row):
            pass

    trm = TestRowMapper()


def test_connection_is_none():
    m = Mapper(None, 0)
    assert m.connection is None


def test_connection_is_test():
    m = Mapper("test", 0)
    assert m.connection == "test"


def test_depth_is_n1():
    m = Mapper(None, -1)
    assert m.depth == -1
    assert m.expand
    assert m.next_depth == -2


def test_depth_is_0():
    m = Mapper(None, 0)
    assert m.depth == 0
    assert not m.expand
    assert m.next_depth == 0


def test_depth_is_1():
    m = Mapper(None, 1)
    assert m.depth == 1
    assert m.expand
    assert m.next_depth == 0


def test_depth_is_2():
    m = Mapper(None, 2)
    assert m.depth == 2
    assert m.expand
    assert m.next_depth == 1


def test_maprow():
    m = Mapper(None, 2)
    m.maprow("test")
    assert m.last_row == "test"
