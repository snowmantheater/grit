import pytest
from gritdb import Entity


class Alpha(Entity):
    def __init__(self):
        super().__init__("alpha")
        self.column("key", key=True)
        self.column("req", required=True)
        self.column("opt")


@pytest.fixture(scope="function")
def xalpha():
    return Alpha()


@pytest.fixture(scope="function")
def alpha():
    alpha = Alpha()
    alpha['key'] = "key"
    alpha['req'] = "required field"
    alpha['opt'] = "optional field"
    return alpha


def test_new():
    alpha = Alpha()
    assert not alpha['key']
    assert not alpha['req']
    assert not alpha['opt']


def test_in(alpha):
    assert "key" in alpha
    assert "foo" not in alpha


def test_get(alpha):
    v = alpha['key']
    assert v == "key"

    with pytest.raises(IndexError):
        v = alpha['foo']


def test_set(alpha):
    alpha['key'] = "hello"
    assert alpha['key'] == "hello"

    with pytest.raises(IndexError):
        alpha['foo'] = "test"


def test_validate_where_instance_is_empty(xalpha):
    with pytest.raises(ValueError):
        xalpha.validate()


def test_validate_where_instance_is_populated(alpha):
    alpha.validate()


def test_str_where_instance_is_empty(xalpha):
    s = str(xalpha)
    assert s == "Entity[alpha](key: None, req: None, opt: None)"


def test_str_where_instance_is_populated(alpha):
    s = str(alpha)
    assert s == "Entity[alpha](key: key, req: required field, opt: optional field)"


def test_params(alpha):
    p = alpha.params()
    assert p == {}


def test_params_where_keys_is_true(alpha):
    p = alpha.params(keys=True)
    assert len(p) == 1
    assert p['key'] == "key"
    assert 'req' not in p
    assert 'opt' not in p


def test_params_where_fields_is_true(alpha):
    p = alpha.params(fields=True)
    assert len(p) == 2
    assert p['req'] == "required field"
    assert p['opt'] == "optional field"
    assert 'key' not in p


def test_params_where_keys_and_fields_is_true(alpha):
    p = alpha.params(keys=True, fields=True)
    assert len(p) == 3
    assert p['key'] == "key"
    assert p['req'] == "required field"
    assert p['opt'] == "optional field"


def test_column_names(xalpha):
    c = xalpha.column_names
    assert len(c) == 3
    assert 'key' in c
    assert 'req' in c
    assert 'opt' in c
