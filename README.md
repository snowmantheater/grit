# Grit

## Description

Grit is simple, utility layer on top of SQL Alchemy Core.

## Getting Started

### Create a Virtual Environment

From the root directory of the projects, execute:

```sh
$ python3 -m venv .venv
```

### Install Requirements

Activate the virtual environment and execute:

```sh
$ pip install -r requirements.txt
```

## Tutorial

This project comes with a demo usage of `gritdb`. It is in the top level directory `grit/demo/`. The database details
for this demo are in the `grit/doc/` directory. Both DDL (for sqlite) and an ERD (in `graphml`) are available. There are
tests for both `gritdb` and the `demo.hr` packages to provide functional examples of usage. Below are some instructions
using the demo code as examples.

### Entities

An Entity generally represents a table, but more specifically represents a result set of a query. It is easier for most
cases to think of it as a table. Here is the `Region` `Entity` from the demo:

```python
class Region(Entity):
    def __init__(self):
        super().__init__("region")
        self.column("region_id", size=11, key=True)
        self.column("region_name", size=25, required=True)
```

1. Create an `Entity` subclass
2. You could theoretically take anything you wanted, here, but `Region` is empty
3. Call the `super` `__init__` with the entity's default name; should match what's in your SqlLayout files
4. Create a column called `region_id` with a `size` of `11` and marked as a `key`
5. Createa a column called `region_name` with a `size` of `25` and marked as `required`

Marking a column as a key makes it required for general validation as well as delete validation. Marking it as required
makes it required for general validation and insert but not necessarily for delete. If a key does not have an auto
sequence in the database and must be specified it can be marked as a `key` _and_ as `required`.

Sizes only make sense for columns that are numeric or strings. If it is numeric, like a `decimal` or `float`, then a two
part size (`(10,2)`) can be used to specify the length on the left and right sides of the decimal point. If two parts
are used in the `size` and the value is not decimal, it will fail for that reason.

#### Relationships

##### Singular

Sometimes Entities related to each other. This is usually used for joins in SQL. An Entity can specify that childe
Entity has a relationship with it by using `relates`. A `Singular` relationhsip means that only one child exists for the
parent. So, the parent will have a foreign key (or keys) which points to the child's primary key (or keys). An example
is the `Country` `Entity`.

```python
class Country(Entity):
    def __init__(self):
        super().__init__("country")
        self.column('country_id', size=2, required=True, key=True)
        self.column('country_name', size=40, required=True)
        self.column('region', required=True, related=Singular('region_id'))
```

Line 6 creates a column called "region" that has a `Singular` relationship on the foreign key `region_id`. This means
that the value of this column is an `Entity` (likely `Region`) and its `region_id` column is referenced by the parent's
(`Country`) `region_id` column. If the parent's foreign key was a different name, then a tuple of two strings can be
provided as a key to `Singular` where the first value is the child entity's primary key name and the second vlaue is the
parent entity's foreign key's name.

##### Plural

If the relationship between child entity to parent entity is many to one, a `Plural` relationship is used. This means
that the parent's key (or keys) is referenced by the child's foreign key (or keys). An example is the `Employee`
`Entity` which has many `Dependent` children entities.

Here is the `Dependent`:

```python
class Dependent(Entity):
    def __init__(self):
        super().__init__("dependent")
        self.column('dependent_id', size=11, key=True)
        self.column('first_name', size=50, required=True)
        ...
        self.column('employee_id', size=11, required=True)
```

Here is the `Employee`:

```python
class EmployeeValidator(EntityValidator):
    def validate(self, entity, create, delete):
        if entity['hire_date'] is not None and type(entity['hire_date']) is not date:
            raise ValueError("Employee['hire_date'] must be of type 'date'")
        
class Employee(Entity):
    def __init__(self):
        super().__init__("employee")
        self.column('employee_id', size=11, key=True)
        self.column('first_name', size=20)
        ...
        self.column('manager', related=Singular(('employee_id', 'manager_id')))
        ...
        self.column('dependents', [], related=Plural('employee_id'))
        self.validator(EmployeeValidator())
```

The `Dependent` defines a column named `employee_id` and the `Employee` defines a column called `dependents` which has
an initial value of a list. An `Entity` has an operation called `reparent` that takes a column to perform on and then
assigns all of the children's foreign keys to the parents primary key.

Here you can also see that the `Employee` has a back reference to itself named `manager_id` that represents another
`Employee` entity's `employee_id`. So, the key is a tuple of two strings.

#### Validation

If more validation that provided by default is necessary for an `Entity`, you can create a subclass of `EntityValidator`
that will be called. The `Employee` class above defines this. This extra piece of validation is to ensure that the type
of one of the columns is a date. You add the validator(s) by calling the `validator` method (similar to adding columns
by calling the `column` method). It is recommended you create one validator for all your needs, but nesting them may be
prefereable for certain types of subclassing.

### RowMappers

If the entity requires more logic than just mapping values from a `Row`, you can create a `RowMapper` subclass. You
instantiate one with the connection and depth provided by the `transactional` decorator along with anything else you
need extra.

```python
class CountryRowMapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self._region_dao = RegionDao(None)  # Transactional only

    def maprow(self, row):
        country = Country()
        country['country_id'] = row.country_id
        country['country_name'] = row.country_name

        if self.expand:
            region = self._region_dao.find_by_id(row.region_id, tx=self.connection, depth=self.next_depth)
        else:
            region = Region()
            region['region_id'] = row.region_id

        country['region'] = region
        return country
```

The constructor should take a `connection` and `depth` value which will come from the `transactional`. These should be
passed directly to the `super`. Next, you can declare extra elements you need to perform mapping. The `maprow` method
takes a SqlAlchemy `Row` object that will have the results of a query as top level, instance attributes. `Entity`
mappers like `CountryRowMapper` can also look to see if they've been requested to expand (read lower levels) and call
`Dao`s for those entities with the provided connection. If they are not expanding, they should create a stub and fill in
the id so that the caller can manually populate the values if needed.

### SqlLayout

SQL is loaded into the system using the `SqlLayout` class. It can read a specialized form of SQL where comments define
headers.

Here is an example from the `RegionDao`:

```sql
-- CREATE region

INSERT INTO region (region_name) VALUES (:region_name);

-- READ region

SELECT region_id, region_name FROM region;

-- READ region (region_id)

SELECT region_id, region_name
FROM region
WHERE region_id = :region_id
;

-- UPDATE region (region_id)

UPDATE region
SET region_name = :region_name
WHERE region_id = :region_id
;

-- DELETE region (region_id)

DELETE FROM region WHERE region_id = :region_id;
```

+ The first statement is defined as a "CREATE" statement for an entity named "region". There are no criteria, because an
  insert statement usually doesn't have criteria.

+ The second statement is a "READ" statement for the entity named "region". Without criteria, this is implied to be a
  "read-all" statement.

+ The third statement is another "READ" statement for the entity named "region". However, there is one key in the
  criteria which means any row matching this criteria is expected to be returned. We can assume because it is the
  table's primary key that only one can come back, but we can search by any criteria, including non-keys, thereby
  getting multiple rows.

+ The forth statement is an "UPDATE" for "region" with one criteria (the primary key).

+ The last statement is a "DELETE" statement for "region". It has a criteria (likely a good idea) which is the primary
  key of the table.

As you can see, you define the type of call, which entity name and then criteria. A unique statement is defined by these
three elements. This lets you have multiple read calls that all search by different parameters.

The SQL Layout is loaded from either a `Path` or a string of a path.

### Daos

The class that wires this all together is the `Dao` base class. A lot of the grunt work is taken out of mix by this base
class 

## License

This project is licensed under the MIT License.

