from datetime import date
from gritdb import Entity, EntityValidator, Plural, Singular


class EmployeeValidator(EntityValidator):
    def validate(self, entity, create, delete):
        if entity['hire_date'] is not None and type(entity['hire_date']) is not date:
            raise ValueError("Employee['hire_date'] must be of type 'date'")


class Employee(Entity):
    def __init__(self):
        super().__init__("employee")
        self.column('employee_id', size=11, key=True)
        self.column('first_name', size=20)
        self.column('last_name', size=25, required=True)
        self.column('email', size=100, required=True)
        self.column('phone_number', size=20)
        self.column('hire_date', required=True)
        self.column('job', required=True, related=Singular('job_id'))
        self.column('salary', size=(8, 2), required=True)
        self.column('manager', related=Singular(('employee_id', 'manager_id')))
        self.column('department', required=True, related=Singular('department_id'))
        self.column('dependents', [], related=Plural('employee_id'))
        self.validator(EmployeeValidator())
