from gritdb import Entity


class Dependent(Entity):
    def __init__(self):
        super().__init__("dependent")
        self.column('dependent_id', size=11, key=True)
        self.column('first_name', size=50, required=True)
        self.column('last_name', size=50, required=True)
        self.column('relationship', size=25, required=True)
        self.column('employee_id', size=11, required=True)
