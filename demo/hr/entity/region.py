from gritdb import Entity


class Region(Entity):
    def __init__(self):
        super().__init__("region")
        self.column("region_id", size=11, key=True)
        self.column("region_name", size=25, required=True)
