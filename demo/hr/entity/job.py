from gritdb import Entity


class Job(Entity):
    def __init__(self):
        super().__init__("job")
        self.column('job_id', size=11, key=True)
        self.column('job_title', size=35, required=True)
        self.column('min_salary', size=(8, 2), required=True)
        self.column('max_salary', size=(8, 2), required=True)
