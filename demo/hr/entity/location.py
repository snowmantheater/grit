from gritdb import Entity, Singular


class Location(Entity):
    def __init__(self):
        super().__init__("location")
        self.column('location_id', size=11, key=True)
        self.column('street_address', size=40)
        self.column('postal_code', size=12)
        self.column('city', size=30, required=True)
        self.column('state_province', size=25)
        self.column('country', required=True, related=Singular('country_id'))
