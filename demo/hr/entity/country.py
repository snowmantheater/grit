from gritdb import Entity, Singular


class Country(Entity):
    def __init__(self):
        super().__init__("country")
        self.column('country_id', size=2, required=True, key=True)
        self.column('country_name', size=40, required=True)
        self.column('region', required=True, related=Singular('region_id'))
