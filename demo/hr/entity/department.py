from gritdb import Entity, Singular


class Department(Entity):
    def __init__(self):
        super().__init__("department")
        self.column('department_id', size=11, key=True)
        self.column('department_name', size=30, required=True)
        self.column('location', required=True, related=Singular('location_id'))
