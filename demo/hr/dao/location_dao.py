from pathlib import Path
from gritdb import Dao, SqlLayout, RowMapper, transactional
from ..entity import Location, Country
from .country_dao import CountryDao


class LocationRowMapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self.country_dao = CountryDao(None)  # Transactional only

    def maprow(self, row):
        location = Location()
        location['location_id'] = row.location_id
        location['street_address'] = row.street_address
        location['postal_code'] = row.postal_code
        location['city'] = row.city
        location['state_province'] = row.state_province

        if self.expand:
            country = self.country_dao.find_by_id(row.country_id, tx=self.connection, depth=self.next_depth)
        else:
            country = Country()
            country['country_id'] = row.country_id

        location['country'] = country
        return location


class LocationDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "location.dao.sql")
        super().__init__(engine, "location", sql)

    @transactional
    def create(self, connection, depth, location):
        location.validate(create=True)
        params = location.params(fields=True)
        location_id = self.exec_create(connection, params)
        location['location_id'] = location_id
        return location

    @transactional
    def find_all(self, connection, depth):
        mapper = LocationRowMapper(connection, depth)
        return self.exec_read(connection, mapper=mapper)

    @transactional
    def find_by_id(self, connection, depth, location_id):
        criteria = {'location_id': location_id}
        mapper = LocationRowMapper(connection, depth)
        return self.exec_read_first(connection, criteria, mapper=mapper)

    @transactional
    def update(self, connection, depth, location):
        location.validate()
        criteria = location.params(keys=True)
        params = location.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, location):
        location.validate(delete=True)
        criteria = location.params(keys=True)
        return self.exec_delete(connection, criteria)
