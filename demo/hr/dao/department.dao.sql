-- =============================================================================
--                                                                    DEPARTMENT
-- =============================================================================

-- CREATE department

INSERT INTO department(
    department_name, location_id
) VALUES (
             :department_name, :location_id
         );

-- READ department

SELECT department_id, department_name, location_id
FROM department
;

-- READ department (department_id)

SELECT department_id, department_name, location_id
FROM department
WHERE department_id = :department_id
;

-- UPDATE department (department_id)

UPDATE department
SET department_name = :department_name, location_id = :location_id
WHERE department_id = :department_id
;

-- DELETE department (department_id)

DELETE FROM department WHERE department_id = :department_id;
