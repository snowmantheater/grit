-- =============================================================================
--                                                                       COUNTRY
-- =============================================================================

-- CREATE country

INSERT INTO country (
    country_id, country_name, region_id
) VALUES (
             :country_id, :country_name, :region_id
         );

-- READ country

SELECT country_id, country_name, region_id
FROM country
;

-- READ country (country_id)

SELECT country_id, country_name, region_id
FROM country
WHERE country_id = :country_id
;

-- UPDATE country (country_id)

UPDATE country
SET country_name = :country_name, region_id = :region_id
WHERE country_id = :country_id
;

-- DELETE country (country_id)

DELETE FROM country WHERE country_id = :country_id;

