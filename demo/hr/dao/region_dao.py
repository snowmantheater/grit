from pathlib import Path
from gritdb import Dao, SqlLayout, transactional
from ..entity import Region


class RegionDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "region.dao.sql")
        super().__init__(engine, "region", sql)

    @transactional
    def create(self, connection, depth, region):
        region.validate(create=True)
        params = region.params(fields=True)
        region_id = self.exec_create(connection, params)
        region['region_id'] = region_id
        return region_id

    @transactional
    def find_all(self, connection, depth):
        return self.exec_read(connection, entity_class=Region)

    @transactional
    def find_by_id(self, connection, depth, region_id):
        criteria = {'region_id': region_id}
        return self.exec_read_first(connection, criteria, entity_class=Region)

    @transactional
    def update(self, connection, depth, region):
        region.validate()
        criteria = region.params(keys=True)
        params = region.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, region):
        region.validate(delete=True)
        criteria = region.params(keys=True)
        return self.exec_delete(connection, criteria)
