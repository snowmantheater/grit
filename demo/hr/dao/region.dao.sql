-- =============================================================================
--                                                                        REGION
-- =============================================================================

-- CREATE region

INSERT INTO region (region_name) VALUES (:region_name);

-- READ region

SELECT region_id, region_name FROM region;

-- READ region (region_id)

SELECT region_id, region_name
FROM region
WHERE region_id = :region_id
;

-- UPDATE region (region_id)

UPDATE region
SET region_name = :region_name
WHERE region_id = :region_id
;

-- DELETE region (region_id)

DELETE FROM region WHERE region_id = :region_id;

