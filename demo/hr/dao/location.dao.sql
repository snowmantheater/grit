-- =============================================================================
--                                                                      LOCATION
-- =============================================================================

-- CREATE location

INSERT INTO location(
    street_address, postal_code, city, state_province, country_id
) VALUES (
             :street_address, :postal_code, :city, :state_province, :country_id
         );

-- READ location

SELECT location_id, street_address, postal_code, city, state_province, country_id
FROM location
;

-- READ location (location_id)

SELECT location_id, street_address, postal_code, city, state_province, country_id
FROM location
WHERE location_id = :location_id
;

-- UPDATE location (location_id)

UPDATE location
SET street_address = :street_address, postal_code = :postal_code, city = :city,
    state_province = :state_province, country_id = :country_id
WHERE location_id = :location_id
;

-- DELETE location (location_id)

DELETE FROM location WHERE location_id = :location_id;
