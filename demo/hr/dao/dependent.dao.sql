-- =============================================================================
--                                                                     DEPENDENT
-- =============================================================================

-- CREATE dependent

INSERT INTO dependent (
    first_name, last_name, relationship, employee_id
) VALUES (
             :first_name, :last_name, :relationship, :employee_id
         );

-- READ dependent

SELECT dependent_id, first_name, last_name, relationship, employee_id
FROM dependent
;

-- READ dependent (dependent_id)

SELECT dependent_id, first_name, last_name, relationship, employee_id
FROM dependent
WHERE dependent_id = :dependent_id
;

-- READ dependent (employee_id)

SELECT dependent_id, first_name, last_name, relationship, employee_id
FROM dependent
WHERE employee_id = :employee_id
;

-- UPDATE dependent (dependent_id)

UPDATE dependent
SET first_name = :first_name, last_name = :last_name, relationship = :relationship,
    employee_id = :employee_id
WHERE dependent_id = :dependent_id
;

-- DELETE dependent (dependent_id)

DELETE FROM dependent WHERE dependent_id = :dependent_id;
