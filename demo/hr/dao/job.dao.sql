-- =============================================================================
--                                                                           JOB
-- =============================================================================

-- CREATE job

INSERT INTO job (
    job_title, min_salary, max_salary
)  VALUES (
              :job_title, :min_salary, :max_salary
          );

-- READ job

SELECT job_id, job_title, min_salary, max_salary
FROM job
;

-- READ job (job_id)

SELECT job_id, job_title, min_salary, max_salary
FROM job
WHERE job_id = :job_id
;

-- UPDATE job (job_id)

UPDATE job
SET job_title = :job_title, min_salary = :min_salary, max_salary = :max_salary
WHERE job_id = :job_id
;

-- DELETE job (job_id)

DELETE FROM job WHERE job_id = :job_id;
