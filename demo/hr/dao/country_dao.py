from pathlib import Path
from gritdb import Dao, RowMapper, SqlLayout, transactional
from .region_dao import RegionDao
from ..entity import Country, Region


class CountryRowMapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self._region_dao = RegionDao(None)  # Transactional only

    def maprow(self, row):
        country = Country()
        country['country_id'] = row.country_id
        country['country_name'] = row.country_name

        if self.expand:
            region = self._region_dao.find_by_id(row.region_id, tx=self.connection, depth=self.next_depth)
        else:
            region = Region()
            region['region_id'] = row.region_id

        country['region'] = region
        return country


class CountryDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "country.dao.sql")
        super().__init__(engine, "country", sql)
        self._region_dao = RegionDao(None)

    @transactional
    def create(self, connection, depth, country):
        country.validate(create=True)
        params = country.params(keys=True, fields=True)
        self.exec_create(connection, params)
        return country['country_id']

    @transactional
    def find_all(self, connection, depth):
        mapper = CountryRowMapper(connection, depth)
        return self.exec_read(connection, mapper=mapper)

    @transactional
    def find_by_id(self, connection, depth, country_id):
        criteria = {'country_id': country_id}
        mapper = CountryRowMapper(connection, depth)
        return self.exec_read_first(connection, criteria, mapper=mapper)

    @transactional
    def update(self, connection, depth, country):
        country.validate()
        criteria = country.params(keys=True)
        params = country.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, country):
        country.validate(delete=True)
        criteria = country.params(keys=True)
        return self.exec_delete(connection, criteria)
