from datetime import date
from pathlib import Path
from gritdb import Dao, SqlLayout, RowMapper, transactional
from ..entity import Employee, Job, Department
from ..dao import DependentDao, JobDao, DepartmentDao


class EmployeeRowMapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self.dependent_dao = DependentDao(None)  # Transactional only
        self.department_dao = DepartmentDao(None)  # Transactional only
        self.job_dao = JobDao(None)  # Transactional only

    def maprow(self, row):
        employee = Employee()
        employee['employee_id'] = row.employee_id
        employee['first_name'] = row.first_name
        employee['last_name'] = row.last_name
        employee['email'] = row.email
        employee['phone_number'] = row.phone_number
        employee['hire_date'] = date.fromisoformat(row.hire_date)
        employee['salary'] = row.salary

        if self.expand:
            department = self.department_dao.find_by_id(row.department_id, tx=self.connection, depth=self.next_depth)
            job = self.job_dao.find_by_id(row.job_id, tx=self.connection, depth=self.next_depth)
            dependents = self.dependent_dao.find_by_employee_id(row.employee_id, tx=self.connection)
        else:
            department = Department()
            department['department_id'] = row.department_id
            job = Job()
            job['job_id'] = row.job_id
            dependents = []

        # This avoids accidental recursion; let the caller populate the manager
        if row.manager_id:
            manager = Employee()
            manager['employee_id'] = row.manager_id
            employee['manager'] = manager

        employee['department'] = department
        employee['job'] = job
        employee['dependents'] = dependents

        return employee


class EmployeeDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "employee.dao.sql")
        super().__init__(engine, "employee", sql)
        self.dependent_dao = DependentDao(None)

    def _upsert_dependents(self, connection, depth, employee):
        affected = 0
        if self.expand(depth):
            employee.reparent('dependents')
            for dependent in employee['dependents']:
                if dependent['dependent_id']:
                    affected += self.dependent_dao.update(dependent, tx=connection, depth=depth)
                else:
                    self.dependent_dao.create(dependent, tx=connection, depth=depth)
                    affected += 1
        return affected

    def _delete_dependents(self, connection, depth, employee):
        affected = 0
        if self.expand(depth):
            for dependent in employee['dependents']:
                if dependent['dependent_id']:
                    affected += self.dependent_dao.delete(dependent, tx=connection, depth=depth)
        return affected

    @transactional
    def create(self, connection, depth, employee):
        employee.validate(create=True)
        params = employee.params(fields=True)
        employee_id = self.exec_create(connection, params)
        employee['employee_id'] = employee_id
        if employee_id:
            self._upsert_dependents(connection, depth, employee)
        return employee

    @transactional
    def find_all(self, connection, depth):
        mapper = EmployeeRowMapper(connection, depth)
        return self.exec_read(connection, mapper=mapper)

    @transactional
    def find_by_id(self, connection, depth, employee_id):
        criteria = {'employee_id': employee_id}
        mapper = EmployeeRowMapper(connection, depth)
        return self.exec_read_first(connection, criteria, mapper=mapper)

    @transactional
    def update(self, connection, depth, employee):
        employee.validate()
        criteria = employee.params(keys=True)
        params = employee.params(fields=True)
        employees_affected = self.exec_update(connection, criteria, params)
        dependents_affected = 0
        if employees_affected:
            dependents_affected = self._upsert_dependents(connection, depth, employee)
        return employees_affected, dependents_affected

    @transactional
    def delete(self, connection, depth, employee):
        employee.validate(delete=True)
        criteria = employee.params(keys=True)
        employees_affected = self.exec_delete(connection, criteria)
        dependents_affected = 0
        if employees_affected:
            dependents_affected = self._delete_dependents(connection, depth, employee)
        return employees_affected, dependents_affected
