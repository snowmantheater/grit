from pathlib import Path
from gritdb import Dao, SqlLayout, RowMapper, transactional
from ..entity import Department, Location
from .location_dao import LocationDao


class DepartmentRowMapper(RowMapper):
    def __init__(self, connection, depth):
        super().__init__(connection, depth)
        self.location_dao = LocationDao(None)  # Transactional only

    def maprow(self, row):
        department = Department()
        department['department_id'] = row.department_id
        department['department_name'] = row.department_name

        if self.expand:
            location = self.location_dao.find_by_id(row.location_id, tx=self.connection, depth=self.next_depth)
        else:
            location = Location()
            location['location_id'] = row.location_id

        department['location'] = location
        return department


class DepartmentDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "department.dao.sql")
        super().__init__(engine, "department", sql)

    @transactional
    def create(self, connection, depth, department):
        department.validate(create=True)
        params = department.params(fields=True)
        department_id = self.exec_create(connection, params)
        department['department_id'] = department_id
        return department

    @transactional
    def find_all(self, connection, depth):
        mapper = DepartmentRowMapper(connection, depth)
        return self.exec_read(connection, mapper=mapper)

    @transactional
    def find_by_id(self, connection, depth, department_id):
        criteria = {'department_id': department_id}
        mapper = DepartmentRowMapper(connection, depth)
        return self.exec_read_first(connection, criteria, mapper=mapper)

    @transactional
    def update(self, connection, depth, department):
        department.validate()
        criteria = department.params(keys=True)
        params = department.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, department):
        department.validate(delete=True)
        criteria = department.params(keys=True)
        return self.exec_delete(connection, criteria)
