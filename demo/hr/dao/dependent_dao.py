from pathlib import Path
from gritdb import Dao, SqlLayout, transactional
from ..entity import Dependent


class DependentDao(Dao):
    def __init__(self, engine):
        sql = SqlLayout(Path(__file__).parent / "dependent.dao.sql")
        super().__init__(engine, "dependent", sql)

    @transactional
    def create(self, connection, depth, dependent):
        dependent.validate(create=True)
        params = dependent.params(fields=True)
        dependent_id = self.exec_create(connection, params)
        dependent['dependent_id'] = dependent_id
        return dependent

    @transactional
    def find_all(self, connection, depth):
        return self.exec_read(connection, entity_class=Dependent)

    @transactional
    def find_by_id(self, connection, depth, dependent_id):
        criteria = {'dependent_id': dependent_id}
        return self.exec_read_first(connection, criteria, entity_class=Dependent)

    @transactional
    def find_by_employee_id(self, connection, depth, employee_id):
        criteria = {'employee_id': employee_id}
        return self.exec_read(connection, criteria, entity_class=Dependent)

    @transactional
    def update(self, connection, depth, dependent):
        dependent.validate()
        criteria = dependent.params(keys=True)
        params = dependent.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, dependent):
        dependent.validate(delete=True)
        criteria = dependent.params(keys=True)
        return self.exec_delete(connection, criteria)
