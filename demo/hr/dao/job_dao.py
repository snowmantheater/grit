from pathlib import Path
from gritdb import Dao, SqlLayout, transactional
from ..entity import Job


class JobDao(Dao):
    def __init__(self, engine):
        sql_plan = SqlLayout(Path(__file__).parent / "job.dao.sql")
        super().__init__(engine, "job", sql_plan)

    @transactional
    def create(self, connection, depth, job):
        job.validate(create=True)
        params = job.params(fields=True)
        job_id = self.exec_create(connection, params)
        job['job_id'] = job_id
        return job

    @transactional
    def find_all(self, connection, depth):
        return self.exec_read(connection, entity_class=Job)

    @transactional
    def find_by_id(self, connection, depth, job_id):
        criteria = {'job_id': job_id}
        return self.exec_read_first(connection, criteria, entity_class=Job)

    @transactional
    def update(self, connection, depth, job):
        job.validate()
        criteria = job.params(keys=True)
        params = job.params(fields=True)
        return self.exec_update(connection, criteria, params)

    @transactional
    def delete(self, connection, depth, job):
        job.validate(delete=True)
        criteria = job.params(keys=True)
        return self.exec_delete(connection, criteria)
