-- =============================================================================
--                                                                      EMPLOYEE
-- =============================================================================

-- CREATE employee

INSERT INTO employee (
    first_name, last_name, email, phone_number, hire_date, job_id, salary,
    manager_id, department_id
) VALUES (
             :first_name, :last_name, :email, :phone_number, :hire_date, :job_id, :salary,
             :manager_id, :department_id
         );

-- READ employee

SELECT employee_id, first_name, last_name, email, phone_number, hire_date,
       job_id, salary, manager_id, department_id
FROM employee
;

-- READ employee (employee_id)

SELECT employee_id, first_name, last_name, email, phone_number, hire_date,
       job_id, salary, manager_id, department_id
FROM employee
WHERE employee_id = :employee_id
;

-- UPDATE employee (employee_id)

UPDATE employee
SET first_name = :first_name, last_name = :last_name, email = :email,
    phone_number = :phone_number, hire_date = :hire_date, job_id = :job_id,
    salary = :salary, manager_id = :manager_id, department_id = :department_id
WHERE employee_id = :employee_id
;

-- DELETE employee (employee_id)

DELETE FROM employee WHERE employee_id = :employee_id;
